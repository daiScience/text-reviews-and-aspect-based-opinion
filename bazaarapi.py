# -*- coding: utf-8 -*-
'''
bazaarapi.py
~~~~~~~~~~~~
This module implements download and management of reviews on a website using the bazaarvoice api in Python
This script will download new reviews since previous execution on a website using the bazaarvoice api, all reviews will be downloaded at first execution
'''

import logging   # logging facility
logger_module = logging.getLogger(__name__)
logger_module.setLevel(logging.INFO)

import functools # higher-order functions and operations on callable objects
import time      # time access and conversions
import requests  # http requests for humans
import json      # json encoder and decoder
import sys       # system-specific parameters and functions
import datetime  # basic date and time types
import os        # miscellaneous operating system interfaces
import inspect   # inspect live objects
import errno     # standard errno system symbols
import pandas    # data analysis tool

# replace by empty dictionary if on server side, as address is whitelisted
PROXIES = {}

# bazaarvoice api keys and account settings
BAZAARVOICE_STAGING_KEY = ''
BAZAARVOICE_PRODUCTION_KEY = ''
BAZAARVOICE_ACCOUNT = ''

# dump file naming convention
FORMAT_BASE_FILE = '_BAZAAR_BASE.json'
FORMAT_DAILY_FILE = '_BAZAAR_DAY.json'

def retry(ExceptionToCheck, tries=5, delay=1, backoff=2, logger=None):
    ''' Retry calling the decorated function using an exponential backoff
        http://www.saltycrane.com/blog/2009/11/trying-out-retry-decorator-python/
        original from: http://wiki.python.org/moin/PythonDecoratorLibrary#Retry
    :ExceptionToCheck: the exception or tuple of exceptions to check
    :tries: int, number of times to try (not retry) before giving up
    :delay: int, initial delay between retries in seconds
    :param backoff: int, backoff multiplier e.g. value of 2 will double the delay
    :logger: logger to use. If None then print warnings, not info
    '''
    def deco_retry(f):
        @functools.wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries>1:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck as e:
                    msg = '%s, Retrying in %d seconds...' % (str(e), mdelay)
                    if logger:
                        logger.warning(msg)
                    else:
                        print(msg)
                    if mdelay>0:
                        time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)
        return f_retry
    return deco_retry

def where(depth=1):
    ''' Return location where the code is running '''
    return os.path.dirname(os.path.abspath(inspect.stack()[depth][1])) if depth else None

@retry(Exception, logger=logger_module)
def get_url_offset(url, session=None, offset=0, proxies=PROXIES, logger=None):
    ''' Return one page of review results using bazaarvoice api
    :url: url to query, must not contains offset parameter
    :session: request.Session object. None to initialise
    :offset: int, number of results to offset
    :proxies: dictionary of proxies for requests
    :logger: logger to use. If None then print warnings, no lower level
    '''
    url_offset = url + '&offset=' + str(offset)
    if session is None:
        session = requests.Session()
        session.trust_env = False
    req = session.get(url_offset, proxies=proxies)
    if not req.status_code==200:
        if logger:
            logger.error('Request failed. %s for url %s' % (req.reason, url))
        req.raise_for_status()
    return session, json.loads(req.text)

def get_url(url, proxies=PROXIES, keeper=10000, logger=None):
    ''' Paginate through all pages of review results using bazaarvoice api
    :url: url to query, must not contains offset parameter
    :proxies: dictionary of proxies for requests
    :keeper: gate keeper to avoid infinite looping
    :logger: logger to use. If None then print warnings, no lower level
    '''
    result = {}
    session = None
    for i in range(keeper):
        count = 0 if not result else len(result.get('Results'))
        session, reviews = get_url_offset(url, session, count, proxies=proxies, logger=logger)
        try:
            result.get('Results').extend(reviews.get('Results'))
        except AttributeError: # first iteration
            result = reviews
        if len(result.get('Results'))>=result.get('TotalResults'):
            break
    if i+1==keeper:
        msg = 'Exiting after %s page of api results' % (i+1)
        if logger:
            logger.warning(msg)
        else:
            print(msg)
    return result.get('Results')

def get_reviews(key=BAZAARVOICE_PRODUCTION_KEY, proxies=PROXIES, keeper=10000, chunk=100, logger=None):
    ''' Paginate through api results to collect all reviews using bazaarvoice api
    :key: bazaarvoice api key https://developer.bazaarvoice.com/
    :proxies: dictionary of proxies for requests
    :keeper: gate keeper to avoid infinite looping. Default 10000
    :chunk: number of result per page to return. Default 100 as per bazaarvoice recommendation
    :logger: logger to use. If None then print warnings, no lower level
    '''
    url = 'http://api.bazaarvoice.com/data/reviews.json?apiversion=5.4&passkey=' + key + '&limit=' + str(chunk)
    return get_url(url, proxies=proxies, keeper=keeper, logger=logger)

def get_daily_reviews_depreciated(date=None, key=BAZAARVOICE_PRODUCTION_KEY, proxies=PROXIES, keeper=1000, chunk=100, logger=None):
    ''' Paginate through api results to collect reviews on a given day using bazaarvoice api
        Note: depreciated as Submission time is not an accurate representation of when the review actually appeared in the website, and retrieved via bazaarvoice
    :date: datetime, retrieve reviews on that date, default to yesterday
    :key: bazaarvoice api key https://developer.bazaarvoice.com/
    :proxies: dictionary of proxies for requests
    :keeper: gate keeper to avoid infinite looping
    :chunk: number of result per page to return. Default 100 as per bazaarvoice recommendation
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if date is None:
        date = datetime.datetime.now() - datetime.timedelta(days=1)
    epoch_end = int(datetime.datetime(date.year, date.month, date.day).timestamp())
    epoch_start = int((datetime.datetime(date.year, date.month, date.day) - datetime.timedelta(days=1)).timestamp())
    url = 'http://api.bazaarvoice.com/data/reviews.json?apiversion=5.4&passkey=' + key + '&limit=' + str(chunk)
    url += '&filter=SubmissionTime:gt:' + str(epoch_start) + '&filter=SubmissionTime:lt:' + str(epoch_end)
    return get_url(url, proxies=proxies, keeper=keeper, logger=logger)

def get_daily_reviews(dir=None, format=(FORMAT_BASE_FILE, FORMAT_DAILY_FILE)
                      , key=BAZAARVOICE_PRODUCTION_KEY, proxies=PROXIES, keeper=1000, chunk=100, logger=None):
    ''' Download all reviews and filter out already downloaded review to get new reviews
    :date: datetime, retrieve reviews on that date, default to yesterday
    :dir: load_file parameter, string, location of existing bazaarvoice files
    :format: load_file parameter, string, suffix of file naming
    :key: bazaarvoice api key https://developer.bazaarvoice.com/
    :proxies: dictionary of proxies for requests
    :keeper: gate keeper to avoid infinite looping
    :chunk: number of result per page to return. Default 100 as per bazaarvoice recommendation
    :logger: logger to use. If None then print warnings, no lower level
    '''
    reviews_new = get_reviews(key=key, proxies=proxies, keeper=keeper, chunk=chunk, logger=logger)
    reviews_old = load_file(dir=dir, format=format, logger=logger)
    review_old_id = set([review.get('Id') for review in reviews_old])
    return [review for review in reviews_new if review.get('Id') not in review_old_id]

def to_datetime(date_text):
    ''' Convert date and time text from bazaarvoice api to python datetime '''
    return datetime.datetime.strptime(date_text[:18], '%Y-%m-%dT%H:%M:%S')

def save_file(list_of_dic, datestamp=None, format=FORMAT_DAILY_FILE, dir=None, logger=None):
    ''' Save list of dictionaries into a dump json file
    :list_of_dic: dictionaries to save
    :datestamp: string, prefix for file naming format YYYYMMDD, default to yesterday
    :format: string, suffix for file naming
    :dir: string, location to save file in
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if dir is None:
        dir = where()
    if datestamp is None:
        datestamp = get_datestamp(datetime.datetime.now()-datetime.timedelta(days=1))
    filename = os.path.join(dir, datestamp+format)
    if os.path.isfile(filename):
        bak = filename + '.bak'
        msg = '%s file already exists, moving existing file to %s' % (filename, bak)
        if logger:
            logger.warning(msg)
        else:
            print(msg)
        os.replace(filename, bak)
    with open(filename, 'w') as f:
        json.dump(list_of_dic, f)
    if logger:
        logger.info('data saved as %s' % filename)
    return filename

def list_file(dir=None, datestamp=None, format=(FORMAT_BASE_FILE, FORMAT_DAILY_FILE), logger=None):
    ''' List all bazzarvoice dump file in directory
    :dir: directory or list of directory to list dump file from
    :datestamp: iterable of prefix of file to list
    :format: iterable of naming file format to list
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if dir is None:
        dir = where()
    if isinstance(dir, str):
        if logger:
            logger.debug('Loading dir %s' % dir)
        if datestamp is None:
            files = [os.path.join(dir, file) for file in os.listdir(dir) if file.endswith(format)]
        else:
            files = [os.path.join(dir, file) for file in os.listdir(dir) if file.endswith(format) and file.startswith(datestamp)]
    else:
        files = []
        for d in dir:
            files += list_file(dir=d, datestamp=datstamp, format=format, logger=logger)
    return sorted(files)

def load_file(file=None, sample_size=None, dir=None, format=(FORMAT_BASE_FILE, FORMAT_DAILY_FILE), logger=None):
    ''' Load bazaarvoice reviews from file or list of file, load from dir if file is None
    :file: file or list of file to load
    :sample_size: if file is None or list, maximum amount of review file to load
    :dir: string, if file is None, location to read files from
    :format: if file is None, iterable of format to load
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if dir is None:
        dir = where()
    if file is None:
        files_to_load = list_file(dir=dir, format=format, logger=logger)
        reviews = load_file(file=files_to_load, sample_size=sample_size, dir=dir, format=format, logger=logger)
    elif isinstance(file, str):
        file_to_load = os.path.join(where(), file) if not os.path.isfile(file) and not dir else os.path.join(dir, file) if not os.path.isfile(file) else file
        if not os.path.isfile(file_to_load):
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), file)
        if logger:
            logger.debug('Loading file %s' % file)
        with open(file_to_load, 'r') as f:
            reviews = json.load(f)
    else:
        reviews = []
        for f in file:
            reviews += load_file(file=f, sample_size=sample_size, dir=dir, format=format, logger=logger)
            if sample_size and len(reviews)>=sample_size:
                msg = 'At least %s files loaded. Stopping' % sample_size
                if logger:
                    logger.warning(msg)
                else:
                    print(msg)
                break
    return reviews

def get_datestamp(date, format='%Y%m%d'):
    ''' Helper function to return datestamp YYYYMMDD from a datetime date '''
    return date.strftime(format)

def get_datestamp_n(n, start=1):
    ''' Helper function to return list of n datestamp
    :n: number of days to retrieve datestamp for
    :start: number of days away to look at. Default 1 for starting yesterday
    '''
    return tuple(sorted([get_datestamp(datetime.datetime.now()-datetime.timedelta(days=start)-datetime.timedelta(days=i)) for i in range(n)]))

def get_yesterday(dir=None, key=BAZAARVOICE_PRODUCTION_KEY, proxies=PROXIES, format=FORMAT_DAILY_FILE, logger=None):
    ''' Download and save yesterday's reviews
    :dir: string, if file is None, directory to save file to. Also get_daily_reviews parameter, location of existing bazaarvoice dump files
    :key: get_daily_reviews parameter, bazaarvoice api key https://developer.bazaarvoice.com/
    :proxies: get_daily_reviews parameter, dictionary of proxies for requests
    :format: get_daily_reviews parameter, string, suffix for file naming
    :logger: logger to use. If None then print warnings, no lower level
    '''
    date = datetime.datetime.now() - datetime.timedelta(days=1)
    datestamp = get_datestamp(date)
    data = get_daily_reviews(dir=dir, key=key, proxies=proxies, logger=logger)
    file = save_file(data, format=format, datestamp=datestamp, dir=dir, logger=logger)
    if logger:
        logger.info('Latest reviews downloaded and saved in %s' % file)
    return file

def get_extract_for_click(list_var, dir=None, datestamp=None, sep=',', keeper=10000, logger=None
                          , key=BAZAARVOICE_PRODUCTION_KEY, proxies=PROXIES, format=FORMAT_DAILY_FILE):
    ''' Create a review extract for Qlik/SAS
    :list_var: required, list of variables to retrieve for each reviews
    :dir: string, if file is None, directory to save file to
    :datestamp: string, prefix for file naming format YYYYMMDD, default to yesterday
    :sep: string, field delimiter for the output file. Defaul comma
    :keeper: gate keeper to avoid infinite looping
    :logger: logger to use. If None then print warnings, no lower level
    :key: bazaarvoice api key https://developer.bazaarvoice.com/
    :proxies: dictionary of proxies for requests
    :format: string, suffix for file naming
    '''
    if dir is None:
        dir = where()
    if datestamp is None:
        datestamp = get_datestamp(datetime.datetime.now()-datetime.timedelta(days=1))
    reviews = get_reviews(key=key, proxies=proxies, keeper=keeper, logger=logger)
    reviews_subset = [[review[var] for var in list_var] for review in reviews]
    file = os.path.join(dir, datestamp+format)
    df = pandas.DataFrame(reviews_subset, columns=list_var)
    df.to_csv(file, sep=sep, header=True, index=False)
    if logger:
        logger.info('Extract saved in %s' % file)
    return file

def get_stamps(format_datestamp='%Y%m%d', format_timestamp='%H%M%S'):
    ''' Helper function to get datestamp and timestamp of current time '''
    current = datetime.datetime.now()
    return get_datestamp(current, format_datestamp), get_datestamp(current, format_timestamp)

def save_acknowledgement_file(path, filename, logger=None):
    ''' Helper function to save an acknowledgment file for Qlik/SAS '''
    file = os.path.join(path, filename)
    if logger:
        logger.info('Creating acknowledgment file %s' % file)
    open(file, 'a').close()
    return file

if __name__ == '__main__':

    format = '%(processName)s - %(asctime)s - %(filename)s - %(funcName)s - %(lineno)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=format)

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    #logger.addHandler(logging.StreamHandler())
    logger.info('Execution starts %s' % os.path.realpath(__file__))

    server_path = r'/home/administrator/03-COLLECTED_DATA/01-BAZAARVOICE'
    dir = server_path if os.path.isdir(server_path) else where()
    proxies = {} if os.path.isdir(server_path) else PROXIES
    file = get_yesterday(dir=dir, proxies=proxies, logger=logger)

    logger.info('Execution ends %s' % os.path.realpath(__file__))
