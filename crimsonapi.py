# -*- coding: utf-8 -*-
'''
crimsonapi.py
~~~~~~~~~~~~~
This module implements download and management of posts using the crimson api in Python
This script will download previous day's posts using crimson api
'''

import logging   # logging facility
logger_module = logging.getLogger(__name__)
logger_module.setLevel(logging.INFO)

import os        # miscellaneous operating system interfaces
import requests  # http requests for humans
import datetime  # basic date and time types
import json      # json encoder and decoder
import warnings  # warning control
import copy      # shallow and deep copy operations
import pandas    # data analysis tool

import bazaarapi  # user-defined, reviews using bazaarvoice api

# replace by empty dictionary if on server side, as address is whitelisted
PROXIES = bazaarapi.PROXIES

# Crimson access token and monitor id
CRIMSON_TOKEN = ''
CRIMSON_ID = ''

# dump file naming convention
FORMAT_DAILY_FILE = '_CRIMSON_DAY.json'

def get_post_url(key=CRIMSON_TOKEN, id=CRIMSON_ID, start=None, end=None, full=True):
    ''' Create the crimson url to query to retrieve posts
    :key: str, crimson api token
    :id: str, crimson monitor id
    :start: datetime, inclusive start date to retrieve post from
    :end: date, exclusive end date to retrieve post from
    :full: bool, whether to return all posts, limited to 10k sample, or 500
    '''
    if start is None and end is None:
        start = datetime.datetime.now() - datetime.timedelta(days=2)
        end = start + datetime.timedelta(days=1)
    elif start is None:
        start = end - datetime.timedelta(days=1)
    elif end is None:
        end = start + datetime.timedelta(days=1)
    start_datestamp, end_datestamp = bazaarapi.get_datestamp(start, '%Y-%m-%d'), bazaarapi.get_datestamp(end, '%Y-%m-%d')
    base_url = 'https://api.crimsonhexagon.com/api/monitor/posts?auth='
    return base_url+key+'&id='+id+'&start='+start_datestamp+'&end='+end_datestamp+'&fullContents=true&extendLimit='+str(full).lower()

@bazaarapi.retry(Exception, logger=logger_module)
def get_posts(start=None, end=None, proxies=PROXIES, key=CRIMSON_TOKEN, id=CRIMSON_ID, full=True, logger=None):
    ''' Retrieve all posts from a Crimson monitor using Crimson api
    :start: datetime, inclusive start date to retrieve post from
    :end: date, exclusive end date to retrieve post from
    :proxies: dictionary of proxies for requests
    :key: str, crimson api token
    :id: str, crimson monitor id
    :full: bool, whether to return all posts, limited to 10k sample, or 500
    :logger: logger to use. If None then print warnings, no lower level
    '''
    url = get_post_url(key=key, id=id, start=start, end=end, full=full)
    with warnings.catch_warnings(): # InsecureRequestWarning
        warnings.simplefilter('ignore')
        session = requests.Session()
        session.trust_env = False
        req = session.get(url, proxies=proxies, verify=False)
    if not req.status_code==200:
        if logger:
            logger.error('Request failed. %s for url %s' % (req.reason, url))
        req.raise_for_status()
    return json.loads(req.text).get('posts')

def get_volume_url(key=CRIMSON_TOKEN, id=CRIMSON_ID, start=None, end=None, logger=None):
    ''' Create the crimson url to query to retrieve volume and sentiment results
    :key: str, crimson api token
    :id: str, crimson monitor id
    :start: datetime, inclusive start date to retrieve post from
    :end: date, exclusive end date to retrieve post from
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if start is None and end is None:
        start = datetime.datetime.now() - datetime.timedelta(days=2)
        end = start + datetime.timedelta(days=1)
    elif start is None:
        start = end - datetime.timedelta(days=1)
    elif end is None:
        end = start + datetime.timedelta(days=1)
    start_datestamp, end_datestamp = bazaarapi.get_datestamp(start, '%Y-%m-%d'), bazaarapi.get_datestamp(end, '%Y-%m-%d')
    base_url = 'https://api.crimsonhexagon.com/api/monitor/results?auth='
    return base_url+key+'&id='+id+'&start='+start_datestamp+'&end='+end_datestamp+'&hideExcluded=false'

def get_volume(start=None, end=None, proxies=PROXIES, key=CRIMSON_TOKEN, id=CRIMSON_ID, logger=None):
    ''' Retrieve retrieve volume and sentiment results from a Crimson monitor using Crimson api
    :start: datetime, inclusive start date to retrieve volume results from
    :end: date, exclusive end date to retrieve volume results from
    :proxies: dictionary of proxies for requests
    :key: str, crimson api token
    :id: str, crimson monitor id
    :logger: logger to use. If None then print warnings, no lower level
    '''
    url = get_volume_url(key=key, id=id, start=start, end=end, logger=logger)
    with warnings.catch_warnings(): # InsecureRequestWarning
            warnings.simplefilter('ignore')
            session = requests.Session()
            session.trust_env = False
            req = session.get(url, proxies=proxies, verify=False)
    if not req.status_code==200:
        if logger:
            logger.error('Request failed. %s for url %s' % (req.reason, url))
        req.raise_for_status()
    return json.loads(req.text).get('results')

def structure_volume_day(volume_day, list_var=None, categoryScores='categories', score='volume', categoryName='category'):
    ''' Helper to structure the data coming out of the crimson api
    :volume_day: daily volume result, one element of results of get_volume
    :list_var: list of variable to retrieve from the volume results
    :categoryScores: key within in dictionary categoryScores representing category
    :score: key within in dictionary categoryScores representing score
    :categoryName: key in dictionary for the value dic containing category to score key to value
    '''
    if list_var is None:
        list_var = ['startDate', 'endDate', 'Basic Negative', 'Basic Neutral', 'Basic Positive']
    dic = copy.deepcopy(volume_day)
    dic.update(get_categoryScores(volume_day, categoryScores=categoryScores, score=score, categoryName=categoryName))
    return [dic[var] for var in list_var]

def structure_volume(volume, list_var=None, categoryScores='categories', score='volume', categoryName='category'):
    ''' Helper to structure the data coming out of the crimson api
    :volume: list of daily volume result, results of get_volume
    :list_var: list of variable to retrieve from the volume results
    :categoryScores: key within in dictionary categoryScores representing category
    :score: key within in dictionary categoryScores representing score
    :categoryName: key in dictionary for the value dic containing category to score key to value
    '''
    return [structure_volume_day(volume_day, list_var=list_var, categoryScores=categoryScores, score=score, categoryName=categoryName) for volume_day in volume]

def save_file(list_of_dic, datestamp=None, format=FORMAT_DAILY_FILE, dir=None, logger=None):
    ''' Save list of dictionaries into a dump json file
    :list_of_dic: dictionaries to save
    :datestamp: string, prefix for file naming format YYYYMMDD, default to yesterday
    :format: suffix format to save
    :dir: string, location to save file in
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if dir is None:
        dir = bazaarapi.where()
    return bazaarapi.save_file(list_of_dic, datestamp=datestamp, format=format, dir=dir, logger=logger)

def list_file(dir=None, datestamp=None, format=FORMAT_DAILY_FILE, logger=None):
    ''' List all crimson dump file in directory
    :dir: directory to list dump file from
    :datestamp: iterable of prefix of file to list
    :format: iterable of naming file format to list
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if dir is None:
        dir = bazaarapi.where()
    return bazaarapi.list_file(dir=dir, datestamp=datestamp, format=format, logger=logger)

def load_file(file=None, sample_size=None, dir=None, format=FORMAT_DAILY_FILE, logger=None):
    ''' Load crimson posts from file
    :file: file to load
    :sample_size: if file is None, maximum amount of reviews to load
    :dir: string, if file is None, location to read files from
    :format: if file is None, iterable of format to load
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if dir is None:
        dir = bazaarapi.where()
    return bazaarapi.load_file(file=file, sample_size=sample_size, dir=dir, format=format, logger=logger)

def get_yesterday(dir=None, lag_day=1, key=CRIMSON_TOKEN, id=CRIMSON_ID, format=FORMAT_DAILY_FILE, proxies=PROXIES, full=True, logger=None):
    ''' Download and save yesterday's posts using Crimson api
    :dir: string, if file is None, directory to save file to
    :lag_day: Number of days ago to get tweets for, 1 yesterday, 2 two days ago etc. Default to 1 for yesterday
    :key: str, crimson api token
    :id: str, crimson monitor id
    :format: get_daily_reviews parameter, string, suffix for file naming
    :proxies: dictionary of proxies for requests
    :full: bool, whether to return all posts, limited to 10k sample, or 500
    :logger: logger to use. If None then print warnings, no lower level
    '''
    date = datetime.datetime.now() - datetime.timedelta(days=lag_day)
    datestamp = bazaarapi.get_datestamp(date)
    data = get_posts(end=date, proxies=proxies, key=key, id=id, full=full, logger=logger)
    file = save_file(data, datestamp=datestamp, format=format, dir=dir, logger=logger)
    if logger:
        logger.info('Posts for %s day ago downloaded and saved in %s' % (lag_day, file))
    return file

def get_history(dir=None, lag_day=1, key=CRIMSON_TOKEN, id=CRIMSON_ID, format=FORMAT_DAILY_FILE, proxies=PROXIES, full=True, logger=None):
    ''' Download and save last days
    :dir: string, if file is None, directory to save file to
    :end: end of range to get crimson posts history for, 1 yesterday, 2 two days ago etc
    :key: str, crimson api token
    :id: str, crimson monitor id
    :format: get_daily_reviews parameter, string, suffix for file naming
    :proxies: dictionary of proxies for requests
    :full: bool, whether to return all posts, limited to 10k sample, or 500
    :logger: logger to use. If None then print warnings, no lower level
    '''
    return [get_yesterday(dir=dir, lag_day=i, key=key, id=id, format=format, proxies=proxies, full=full, logger=logger) for i in range(0, end+1)]

def get_categoryScores(post, categoryName='categoryName', score='score', categoryScores='categoryScores'):
    ''' Helper to retrieve nested results within the result of a crimson api
    :post: dictionary of results
    :categoryScores: key within in dictionary categoryScores representing category
    :score: key within in dictionary categoryScores representing score
    :categoryName: key in dictionary for the value dic containing category to score key to value
    '''
    return {l.get(categoryName): l.get(score) for l in post.get(categoryScores)}

def get_extract_for_click(start=None, end=None, list_var=None, dir=None, datestamp=None, sep='|', logger=None
                          , format=FORMAT_DAILY_FILE, proxies=PROXIES, key=CRIMSON_TOKEN, id=CRIMSON_ID):
    ''' Create a volume result extract for Qlik/SAS
    :start: datetime, inclusive start date to retrieve volume results from
    :end: date, exclusive end date to retrieve volume results from
    :list_var: required, list of variables to retrieve for each daily volume results
    :dir: string, if file is None, directory to save file to
    :datestamp: string, prefix for file naming format YYYYMMDD, default to yesterday
    :sep: string, field delimiter for the output file. Defaul comma
    :logger: logger to use. If None then print warnings, no lower level
    :format: get_daily_reviews parameter, string, suffix for file naming
    :proxies: dictionary of proxies for requests
    :key: str, crimson api token
    :id: str, crimson monitor id
    '''
    if start is None:
        start = datetime.datetime(year=2019, month=1, day=1)
    if end is None:
        end = datetime.datetime.now()
    if list_var is None:
        list_var = ['startDate', 'endDate', 'Basic Negative', 'Basic Neutral', 'Basic Positive']
    if dir is None:
        dir = bazaarapi.where()
    volume = get_volume(start=start, end=end, proxies=proxies, key=key, id=id, logger=logger)
    volume_structured = structure_volume(volume, list_var=list_var)
    file = os.path.join(dir, datestamp+format)
    df = pandas.DataFrame(volume_structured, columns=list_var)
    df.to_csv(file, sep=sep, header=True, index=False)
    if logger:
        logger.info('Extract saved in %s' % file)
    return file

if __name__ == '__main__':

    format = '%(processName)s - %(asctime)s - %(filename)s - %(funcName)s - %(lineno)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=format)

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    #logger.addHandler(logging.StreamHandler())
    logger.info('Execution starts %s' % os.path.realpath(__file__))

    server_path = r'/home/administrator/03-COLLECTED_DATA/05-CRIMSON'
    dir = server_path if os.path.isdir(server_path) else bazaarapi.where()
    proxies = {} if os.path.isdir(server_path) else PROXIES
    file = get_yesterday(dir=dir, proxies=proxies, logger=logger)

    logger.info('Execution ends %s' % os.path.realpath(__file__))
