# -*- coding: utf-8 -*-
'''
feedback.py
~~~~~~~~~~~
This module implements feedback retrieval from text data in Python. The main two approaches implemented are
This script creates feedback dump files from bazaar voice and/or twitter dump file
'''

import logging         # logging facility
logger_module = logging.getLogger(__name__)
logger_module.setLevel(logging.INFO)

import spacy           # industrial-strength natural language processing
import re              # regular expression operations
import itertools       # iterators for efficient looping
import os              # miscellaneous operating system interfaces
import errno           # standard errno system symbols
import inspect         # inspect live objects
import sklearn         # machine learning library
import string          # common string operations
import en_core_web_sm  # spacy model, loaded as module https://spacy.io/usage/models
import nltk            # natural language toolkit
import numpy           # multi-dimensional arrays and matrices
import pandas          # data analysis tool
import datetime        # basic date and time types
import Levenshtein     # string edit distances and similarities
import collections     # high-performance container datatypes
import copy            # shallow and deep copy operations
import gensim          # topic modelling for humans
import pickle          # object serialization
import emoji           # emoji for Python
import multiprocessing # process-based parallelism
import pathos          # parallel graph management, execution in heterogeneous computing
import sklearn.feature_extraction # text and image feature extraction

import bazaarapi       # user-defined, reviews using bazaarvoice api
import twitterapi      # user-defined, tweet data

WORD_POSITIVE = 'lexicon_positive.txt'    # list of words with a positive opinion
WORD_NEGATIVE = 'lexicon_negative.txt'    # list of words with a negative opinion
WORD_STOPWORD = 'lexicon_stopwords.txt'   # list of stopwords
PROD_HIERARCHY = 'product_hierarchy.txt'  # product hierarchy, | delimited with header
FORMAT_FILE = '.feedback'                 # dump file naming convention for feedbacks
GOOGLE_SRC = [                            # url of word2vec pre-trained google news corpus
    'https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?usp=sharing'
    , 'https://github.com/mmihaltz/word2vec-GoogleNews-vectors' ]
GOOGLE_BIN = 'google_news_300.bin'        # word2vec pre-trained google news corpus
GOOGLE_PKL = 'google_news_300.pkl'        # dump of gensim load_word2vec_format model

def load_lexicon(file):
    ''' Load a list of words from a flat file '''
    file_to_load = os.path.join(bazaarapi.where(), file) if not os.path.isfile(file) else file
    if not os.path.isfile(file_to_load):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), file)
    with open(file_to_load, 'r') as f:
        load = [line.strip() for line in f]
    return load

def load_product_hierarchy(file=PROD_HIERARCHY):
    '''Load into a dataframe and clean product hierarchy '''
    file_to_load = os.path.join(bazaarapi.where(), file) if not os.path.isfile(file) else file
    if not os.path.isfile(file_to_load):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), file)
    data = pandas.read_csv(file_to_load, sep='|', header=0)
    # product hierarchy have duplicates at item level
    subset = ['Area', 'Product Category', 'Marketing Name', 'Item']
    return data.drop_duplicates(subset=subset, keep='first', inplace=False)

def load_word2vec(file_pkl=GOOGLE_PKL, file_bin=GOOGLE_BIN, source=GOOGLE_SRC, logger=None):
    ''' Load word2vec pre-trained google news corpus into gensim KeyedVectors
    :file_pkl: pickel file containing word2vec pre-trained google news corpus
    :file_bin: binary word2vec pre-trained Google News corpus
    :source: url to retrieve binary word2vec if both file_pkl and file_bin not found
    :logger: logger to use. If None then print warnings, no lower level
    '''
    load_file_pkl = os.path.join(bazaarapi.where(), file_pkl) if not os.path.isfile(file_pkl) else file_pkl
    load_file_bin = os.path.join(bazaarapi.where(), file_bin) if not os.path.isfile(file_bin) else file_bin
    if os.path.isfile(load_file_pkl):
        if logger:
            logger.info('Loading %s' % load_file_pkl)
        with open(load_file_pkl, 'rb') as f:
            model = pickle.load(f)
    elif os.path.isfile(load_file_bin):
        msg = 'File %s not found. Loading and saving %s' % (file_pkl, load_file_bin)
        if logger:
            logger.warning(msg)
        else:
            print(msg)
        model = gensim.models.KeyedVectors.load_word2vec_format(load_file_bin, binary=True)
        with open(load_file_pkl, 'wb') as f:
            pickle.dump(model, f)
    else:
        file_not_found = FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), file)
        raise Exception('Download word2vec binary files; %s' %source) from file_not_found
    return model

def word2vec_similarity(string1, string2, model=None):
    ''' Similarity function two string using word2vec_model. Recommnend wrapping the function using word2vec_similarity_wrapper
    :string1: string representing word or token
    :string2: string representing word or token
    :model: gensim KeyedVectors word2vec model, result of load_word2vec
    '''
    try:
        if not (str(' ') in string1 or str(' ') in string2): # ngrams
            return model.similarity(string1, string2)
        else:
            return model.n_similarity(string1.split(), string2.split())
    except KeyError: # not in vocabulary
        return 0

def word2vec_similarity_wrapper(model=None):
    ''' Helper wrapper for word2vec_similarity
    :model: gensim KeyedVectors word2vec model, result of load_word2vec
    '''
    if model is None:
        model = load_word2vec()
    return lambda string1, string2: word2vec_similarity(string1, string2, model=model)

def clean_str(string):
    ''' String cleaning, specific for tensorflow pipeline
        https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    '''
    string = re.sub(r'[^A-Za-z0-9(),!?\'\`]', ' ', string)
    string = re.sub(r'\'s', ' \'s', string)
    string = re.sub(r'\'ve', ' \'ve', string)
    string = re.sub(r'n\'t', ' n\'t', string)
    string = re.sub(r'\'re', ' \'re', string)
    string = re.sub(r'\'d', ' \'d', string)
    string = re.sub(r'\'ll', ' \'ll', string)
    string = re.sub(r',', ' , ', string)
    string = re.sub(r'!', ' ! ', string)
    string = re.sub(r'\(', ' \( ', string)
    string = re.sub(r'\)', ' \) ', string)
    string = re.sub(r'\?', ' \? ', string)
    string = re.sub(r'\s{2,}', ' ', string)
    return string.lower()

def clean_text(list_text, list_exclude=None, list_start=('http','@','#')):
    ''' Basic implementation of common text preprocessing for a simple pipeline
    :list_text: iterable of text string
    :list_exclude: iterable of words to exclude and filter out
    :list_start: iterable of starting string to ignore and filter out (used with startswith())
    '''
    if list_exclude is none:
        list_exclude = []
    lmtzr = nltk.stem.WordNetLemmatizer()
    rm_punct = lambda x: x.lower().translate(str.maketrans('', '', string.punctuation))
    return [[rm_punct(lmtzr.lemmatize(w)) for w in text.split()
                       if rm_punct(lmtzr.lemmatize(w)) not in list_exclude
                       and not w.startswith(list_start)]
             for text in list_text]

def split_sentence(text, model=None):
    ''' Splits text into a list of text representing sentences
    :text: string representing the review
    :model: spacy model, https://spacy.io/usage/models,
        example: result of import en_core_web_sm; model = en_core_web_sm.load() or model = spacy.load('en')
    '''
    if model is None:
        model = en_core_web_sm.load()
    return [str(sentence) for sentence in model(text).sents] if text else []

def extract_nouns(text, model=None):
    ''' Extract nouns from text representing a sentence
    :text: string representing the review
    :model: spacy model, https://spacy.io/usage/models,
        example: result of import en_core_web_sm; model = en_core_web_sm.load() or model = spacy.load('en')
    '''
    if model is None:
        model = en_core_web_sm.load()
    return [str(token.lemma_) for token in model(text) if token.pos_ in ('NOUN')]

def get_idf(list_text, ngram_range=(1,1), stop_words=None, lowercase=True):
    ''' Compute inverse document frequency
    :list_text: iterable of text string
    :ngram_range: sklearn TfidfVectorizer range of ngrams parameter, default no ngram (1,1)
    :stop_words: sklearn TfidfVectorizer stop_word parameter, default None
    :lowercase: sklearn TfidfVectorizer lowercase parameter, default True
    '''
    param = dict(lowercase=lowercase, ngram_range=ngram_range, stop_words=stop_words)
    vectorizer = sklearn.feature_extraction.text.TfidfVectorizer(**param)
    response = vectorizer.fit_transform(list_text)
    dictionary = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))
    return sorted([(k, v) for k, v in dictionary.items()], key=lambda x: x[1])

def get_opinion(text, model=None, lexicon_neg=None, lexicon_pos=None, logger=None):
    ''' Extract features with an opinion from a sentence
    :sentence: string representing a sentence
    :model: spacy model to consider, https://spacy.io/usage/models
    :lexicon_neg: list of words representing the negative word lexicon
    :lexicon_pos: list of words representing positive word lexicon
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if logger:
        logger.debug('Processing text "%s"' %text)
    if model is None:
        model = en_core_web_sm.load()
    if lexicon_neg is None:
        lexicon_neg = load_lexicon(WORD_NEGATIVE)
    if lexicon_pos is None:
        lexicon_pos = load_lexicon(WORD_POSITIVE)
    result = []
    recall = []
    lexicon = lexicon_neg+lexicon_pos
    if not any(token in text.lower() for token in lexicon):
        if logger:
            logger.debug('No opinion found. Exiting early')
        return result
    for token in model(text):

        if token.text.lower() in lexicon: # if word is opinion word

            sentiment = 1 if token.text.lower() in lexicon_pos else -1

            if logger:
                logger.debug('0. Opinion token found "%s" with sentiment %s' % (token.text, sentiment))
                logger.debug('token.text=%s  and  token.dep_=%s' % (token.text, token.dep_))

            if (token.dep_=='advmod'): # ignore adverb modifier if opinion words
                continue
            elif (token.dep_=='amod'): # if adjective modifier, simply affect to head

                if logger:
                    logger.debug('1. Token is adjective modifier. Affecting to head')
                    logger.debug('child.text=%s  and  child.dep_=%s' % (child.text, child.dep_))

                for child in token.children:

                    # if there's a adj modifier (i.e. very, pretty, etc.) add more weight to sentiment
                    if (child.dep_=='amod' or child.dep_=='advmod') and (child.text.lower() in lexicon):
                        if logger:
                            logger.debug('2. Associated to adjective modifier "%s". Adjusting sentiment score' % child.text)
                            logger.debug('token.text=%s  and  child.text=%s  and  child.dep_=%s' % (token.text, child.text, child.dep_))
                        sentiment *= 1.5

                    if child.dep_=='neg': # flip sentiment if negative word
                        if logger:
                            logger.debug('3. Negation opinion for child "%s". Adjusting sentiment score' % child.text)
                            logger.debug('token.text=%s  and  child.text=%s  and  child.dep_=%s' % (token.text, child.text, child.dep_))
                        sentiment *= -1

                result.append((token.head.text, token.text, sentiment))

            else: # for opinion words that are adjectives, adverbs, verbs

                if logger:
                    logger.debug('4. Token "%s" is one of adjectives, adverbs, verbs' % token.text)
                    logger.debug('token.text=%s  and  token.dep_=%s' % (token.text, token.dep_))

                for child in token.children: # check token children for adjective modifier and negation

                    # if there's a adj modifier (i.e. very, pretty, etc.) add more weight to sentiment
                    if (child.dep_=='amod' or child.dep_=='advmod') and (child.text.lower() in lexicon):
                        if logger:
                            logger.debug('5. Associated to adjective modifier "%s". Adjusting sentiment score' % child.text)
                            logger.debug('token.text=%s  and  child.text=%s  and  child.dep_=%s' % (token.text, child.text, child.dep_))
                        sentiment *= 1.5

                    if child.dep_=='neg': # flip sentiment if negative word
                        if logger:
                            logger.debug('6. Negation opinion for child "%s". Adjusting sentiment score' % child.text)
                            logger.debug('token.text=%s  and  child.text=%s  and  child.dep_=%s' % (token.text, child.text, child.dep_))
                        sentiment *= -1

                for child in token.children: # check any results in children

                    if token.pos_=='VERB' and child.dep_=='dobj': # if verb, check if there's a direct object

                        if logger:
                            logger.debug('7. Token is a verb. Associate to direct object' % child.text)
                            logger.debug('token.text=%s  and  child.text=%s  and  child.dep_=%s' % (token.text, child.text, child.dep_))
                        result.append((child.text, token.text, sentiment))

                        subchildren, conj = [], 0 # check for conjugates (a AND b), then add both to dictionary

                        for subchild in child.children:

                            if subchild.text=='and':
                                conj = 1
                            if conj==1 and not subchild.text=='and':
                                if logger:
                                    logger.debug('8. Conjugate "%s" found for token' % subchild.text)
                                    logger.debug('token.text=%s  and  subchild.text=%s' % (token.text, child.text))
                                result.append((subchild.text, token.text, sentiment))
                                conj = 0

                for child in token.head.children: # check token heads children for adjective modifier and negation

                    if (child.dep_=='amod' or child.dep_=='advmod') and (child.text in lexicon):
                        if logger:
                            logger.debug('9. Token head is associated to adjective modifier "%s". Adjusting sentiment score' % child.text)
                            logger.debug('token.text=%s  and  child.text=%s  and  child.dep_=%s' % (token.text, child.text, child.dep_))
                        sentiment *= 1.5

                    if child.dep_=='neg':
                        if logger:
                            logger.debug('10. Token head has negation opinion for child "%s". Adjusting sentiment score' % child.text)
                            logger.debug('token.text=%s  and  child.text=%s  and  child.dep_=%s' % (token.text, child.text, child.dep_))
                        sentiment *= -1

                for child in token.head.children: # check any results in token head children

                    if not child == token:
                        noun = ''

                        if child.pos_=='NOUN' and child.text not in [aspect for aspect, opinion, sentiment in result]:
                            noun = child.text
                            if logger:
                                logger.debug('11. Token is associated to "%s".' % child.text)
                                logger.debug('token.text=%s  and  child.text=%s  and  child.pos_=%s' % (token.text, child.text, child.pos_))

                            for subchild in child.children: # Check for compound nouns
                                if subchild.dep_=='compound':
                                    noun = subchild.text + ' ' + noun
                                    if logger:
                                        logger.debug('12. Counpoud found "%s".' % subchild.text)
                                        logger.debug('token.text=%s  and  child.text=%s  and  subchild.text=%s' % (token.text, child.text, subchild.text))

                            result.append((noun, token.text, sentiment))

                if token.head.text.lower() in lexicon: # check if head is conjugated opinion
                    if token.dep_=='conj':
                        if logger:
                            logger.debug('13. Possible conjugated opinion found "%s". Adding to recall' % token.head.text)
                        recall.append([token.text, token.head.text, sentiment])

    #for token in recall:
    for token, head, sentiment in recall:
        if logger:
            logger.debug('14. Checking possible conjugated opinion recalled "%s"' % token)

        if head.lower() in [opinion.lower() for aspect, opinion, sentiment in result]:
            _index = [opinion.lower() for aspect, opinion, sentiment in result].index(head.lower())
            result.append([result[_index][0], token, sentiment])

    return result

def check(text, model=None, jupyter=True):
    ''' Debug function, check opinions extracted from text with sentence display
    :text: string representing a sentence
    :model: spacy model to consider, https://spacy.io/usage/models
    :jupyter: whether code runs within jupyter, use render if True, serve is False
    '''
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())
    if model is None:
        model = en_core_web_sm.load()
    text_model = model(text)
    opinion = get_opinion(text, model=model, logger=logger)
    if jupyter:
        spacy.displacy.render(text_model, style='dep', jupyter=True)
    else:
        spacy.displacy.serve(text_model, style='dep')
    return opinion

def get_similarity_score(str1, str2, similarity=None, lowercase=True, lemmatizer=None):
    ''' Get similarity score between two strings
    :str1: first string
    :str2: second string
    :similarity: similarity function, default to Levenshtein.ratio
    :lowercase: whether to lowercase before calculating similarity or not, default True
    :lemmatizer: lemmatizer function to apply, default to None. No lemmatizer if None or False, nltk WordNetLemmatizer() if True
    '''
    if similarity is None:
        similarity = Levenshtein.ratio
    if lemmatizer is None or lemmatizer==False:
        get_lemma = lambda x: x
    elif lemmatizer==True:
        get_lemma = nltk.stem.WordNetLemmatizer().lemmatize
        nltk.corpus.wordnet.ensure_loaded() # https://stackoverflow.com/questions/27433370/
    else:
        get_lemma = lemmatizer
    string1 = str1.lower() if lowercase else str1
    string2 = str2.lower() if lowercase else str2
    string1 = get_lemma(string1)
    string2 = get_lemma(string2)
    return similarity(string1, string2)

def get_bag_string(string, list_string, param, threshold, chunksize=None, include_threshold=True):
    ''' Create a list of similar strings to a given string within list of string. Only search on the right of given list
    :string: base string to compare reminder list of strings to
    :list_string: list of strings. Must contain string argument
    :param: dictionary containing similarity, lowercase, lemmatizer paramater for get_similarity_score
    :threshold: float, similarity threshold to associate strings together
    :include_threshold: boolean, whether the threshold is included in the results. Default True
    :chunksize: int, only search a number of string on the right of string argument. Default None for all of list_string
    '''
    end_range = min(list_string.index(string)+1+chunksize, len(list_string)) if chunksize else len(list_string)
    results =  [(list_string[i], get_similarity_score(string, list_string[i], **param))
                for i in range(list_string.index(string)+1, end_range)
                if get_similarity_score(string, list_string[i], **param)>=threshold]
    return results if include_threshold else [(str, score) for str, score in results if score>threshold]

def get_bag(list_string, param, threshold, chunksize=None, include_threshold=True, thread=True):
    ''' Create a dic of lists collections.defaultdict(list) of similar strings within list of strings. Only search on the right of list_string
    :list_string: list of strings. Must be unique strings, no duplicates allowed
    :param: dictionary containing similarity, lowercase, lemmatizer paramaters for get_similarity_score function
    :threshold: float, similarity threshold to associate strings together
    :chunksize: int, only search a number of string on the right of string argument. Default None for all of list_string
    :include_threshold: boolean, whether the threshold is included in the results. Default True
    :thread: boolean, whether to execute all file in parallel (threading). Default True
    '''
    bag = collections.defaultdict(list)
    if thread:
        pool = pathos.multiprocessing.ThreadingPool()
        bag_list = pool.map(get_bag_string
                            , list_string
                            , [list_string]*len(list_string)
                            , [param]*len(list_string)
                            , [threshold]*len(list_string)
                            , [chunksize]*len(list_string)
                            , [include_threshold]*len(list_string))
        pool.close()
        pool.join()
        pool.clear()
    else:
        bag_list = [get_bag_string(string, list_string, param, threshold, chunksize, include_threshold) for string in list_string]
    if chunksize is not None:
        bag_keys = [list_string[i] for i in range(len(bag_list)) if bag_list[i]]
        bag_temp = get_bag(bag_keys, param, threshold, chunksize=None, include_threshold=include_threshold, thread=False)
        for key in bag_keys:
            index = list_string.index(key)
            bag_list[index] += [(string, get_similarity_score(key, string, **param))
                                for string, score in bag_temp[key]
                                if key in bag_temp]
    for i in range(len(bag_list)):
        if bag_list[i]:
            bag[list_string[i]] = bag_list[i]
    return bag

def unpack_bag(bag, param, pop=True):
    ''' Unpack a bag - dic of lists - when string is key and in an item in a list
    :bag: collections.defaultdict(list), dic of lists, represent bag of similar word. Result of get_bag
    :param: dictionary containing similarity, lowercase, lemmatizer paramaters for get_similarity_score function
    :pop: boolean, whether to pop the keys out of the bag as unpacking. Default True
    '''
    for key in list(bag):
        for string, score in bag[key]:
            if string in bag:
                string_list = bag.pop(string) if pop else bag.get(string)
                bag[key] += [(each, get_similarity_score(key, each, **param)) for each, score in string_list]
    return bag

def get_dic_from_bag(bag, param):
    ''' Return dictionary of value, key for a bag - dic of lists - for each value in lists
    :bag: collections.defaultdict(list), dic of lists, represent bag of similar word. Result of get_bag
    :param: dictionary containing similarity, lowercase, lemmatizer paramaters for get_similarity_score function
    '''
    dic = {}
    for key in bag:
        for string, score in bag[key]:
            if string not in dic:
                dic[string] = key
            elif score>get_similarity_score(string, key, **param):
                dic[string] = key
    return dic

def get_string(list_strings, return_counts=False):
    ''' Return list of unique string in list ordered by occurence '''
    string_count = list(zip(*numpy.unique(list_strings, return_counts=True)))
    string_count = sorted(list(string_count), key= lambda x: x[1], reverse=True)
    if return_counts:
        return [string for string, count in string_count], [count for string, count in string_count]
    return [string for string, count in string_count]

def get_similarity_dic(list_string, threshold=0.8, return_bags=False, similarity=None, lowercase=True, lemmatizer=True
                   , chunksize=None, include_threshold=True, percentile=None):
    ''' Create similarity dictionary for similar but lower occurence string of a list of strings
    :list_string: list or iterable containing strings
    :threshold: float, similarity threshold to associate strings together
    :return_bags: debug option, return step results
    :similarity: get_similarity_score option; similarity function, default to Levenshtein.ratio
    :lowercase: get_similarity_score option; whether to lowercase before calculating similarity or not, default True
    :lemmatizer: get_similarity_score option; lemmatizer function to apply. No lemmatizer if None, nltk WordNetLemmatizer() if True (default)
    :chunksize: get_bag option; int, only search a number of string on the right of string argument. Default None for all strings
    :include_threshold: boolean, whether the threshold is included in the results. Default True
    :percentile: float, exclude tail of distribution from computation, string with counts are below above percentile. Default None for no exclusion
    '''
    if similarity is None:
        similarity = Levenshtein.ratio
    if lemmatizer is None or lemmatizer==False:
        get_lemma = lambda x: x
    elif lemmatizer==True:
        get_lemma = nltk.stem.WordNetLemmatizer().lemmatize
        nltk.corpus.wordnet.ensure_loaded() # https://stackoverflow.com/questions/27433370/
    else:
        get_lemma = lemmatizer
    param = dict(similarity=similarity, lowercase=lowercase, lemmatizer=get_lemma)
    if percentile is not None:
        strings, counts = get_string(list_string, return_counts=True)
        perc = 100*percentile if 0<percentile<1 else percentile
        index_value = numpy.percentile(counts, perc)
        index = counts.index(index_value)
        bag = get_bag(strings[:index], param, threshold, chunksize=chunksize, include_threshold=include_threshold)
    else:
        strings = get_string(list_string)
        bag = get_bag(strings, param, threshold, chunksize=chunksize, include_threshold=include_threshold)
    if return_bags:
        bag_copy = copy.deepcopy(bag)
    bag = unpack_bag(bag, param)
    dic = get_dic_from_bag(bag, param)
    if return_bags:
        return bag_copy, bag, dic
    return dic

def save_file(list_of_feedback, datestamp=None, format=FORMAT_FILE, dir=None, logger=None):
    ''' Save feeback into a dump json file
    :list_of_feedback: dictionary to save
    :datestamp: string, prefix for file naming format YYYYMMDD, default to yesterday
    :format: suffix format to save
    :dir: string, location to save file in
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if datestamp is None:
        date = datetime.datetime.now() - datetime.timedelta(days=1)
        datestamp = bazaarapi.get_datestamp(date)
    if dir is None:
        dir = bazaarapi.where()
    return bazaarapi.save_file(list_of_feedback, datestamp=datestamp, format=format, dir=dir, logger=logger)

def list_file(dir=None, datestamp=None, format=FORMAT_FILE, logger=None):
    ''' List all opinion dump file in directory
    :dir: directory to list dump file from
    :datestamp: iterable of prefix of file to list
    :format: iterable of naming file format to list
    '''
    if dir is None:
        dir = bazaarapi.where()
    return bazaarapi.list_file(dir=dir, datestamp=datestamp, format=format, logger=logger)

def load_file(file=None, sample_size=None, dir=None, format=FORMAT_FILE, logger=None):
    ''' Load feedbacks from files
    :file: file to load
    :sample_size: maximum amount of reviews to load
    :dir: string, location to read files from
    :format: suffix or iterable of suffix format to load
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if dir is None:
        dir = bazaarapi.where()
    return bazaarapi.load_file(file=file, sample_size=sample_size, dir=dir, format=format, logger=logger)

def get_opinion_parameters(model=None, lexicon_neg=None, lexicon_pos=None):
    ''' Load keyword parameters needed for get_opinion '''
    if model is None:
        model = en_core_web_sm.load()
    if lexicon_neg is None:
        lexicon_neg = load_lexicon(WORD_NEGATIVE)
    if lexicon_pos is None:
        lexicon_pos = load_lexicon(WORD_POSITIVE)
    return dict(model=model, lexicon_neg=lexicon_neg, lexicon_pos=lexicon_pos)

def get_feedback(text, id=None, param=None):
    ''' Gather feedback from text
    :text: string representing a document, collection of sentences
    :id: id to identify original text source
    :param: dictionary of keyword parameters for get_opinion. Example get_opinion_parameters
    '''
    if param is None:
        param = get_opinion_parameters()
    return [(id, sentence, get_opinion(sentence, **param)) for sentence in split_sentence(text)]

def get_feedback_from_review(review, param):
    ''' Gather feedback from review
    :review: dictionary representing a review
    :param: dictionary of keyword parameters for get_opinion. Example get_opinion_parameters
    '''
    return get_feedback(review['ReviewText'], review['Id'], param)

def get_feedback_from_tweet(tweet, param):
    ''' Gather feedback from tweet
    :tweet: dictionary representing a tweet
    :param: dictionary of keyword parameters for get_opinion. Example get_opinion_parameters
    '''
    return get_feedback(tweet['full_text'], tweet['id'], param)

def get_feedback_from_posts(post, param):
    ''' Gather feedback from a crimson post
    :post: dictionary representing a post
    :param: dictionary of keyword parameters for get_opinion. Example get_opinion_parameters
    '''
    raise NotImplementedError ## Need to decide on a rule to create an id for every posts

def do_bazaar_file(file, param, logger=None, get_fcn=get_feedback_from_review, multi=True):
    ''' Get feedbacks from a bazaarvoice dump file
    :file: bazaarvoice file to process
    :param: dictionary of keyword parameters for get_opinion. Example get_opinion_parameters
    :logger: logger to use. If None then print warnings, no lower level
    :get_fcn: function to retrieve individual feedback. Default to get_feedback_from_review
    :multi: boolean, whether to execute all file in parallel (processing pool). Default True
    '''
    file_with_path = os.path.join(bazaarapi.where(), file) if file and not os.path.isfile(file) else file
    reviews = bazaarapi.load_file(file=file_with_path)
    if logger:
        logger.info('Processing %s' % file)
    if multi:
        pool = pathos.multiprocessing.ProcessingPool()
        results_dump = pool.map(get_fcn, reviews, [param]*len(reviews))
        pool.close()
        pool.join()
        pool.clear()
    else:
        results_dump = [get_fcn(review, param) for review in reviews]
    results = [result for dump in results_dump for result in dump]
    return results

def do_twitter_file(file, param, logger=None, get_fcn=get_feedback_from_tweet, multi=True):
    ''' Get feedbacks from a twitter dump file
    :file: twitter file to process
    :param: dictionary of keyword parameters for get_opinion. Example get_opinion_parameters
    :get_fcn: function to retrieve individual feedback. Default to get_feedback_from_tweet
    :logger: logger to use. If None then print warnings, no lower level
    :multi: boolean, whether to execute all file in parallel (processing pool). Default True
    '''
    return do_bazaar_file(file, param, get_fcn=get_fcn, logger=logger, multi=multi)

def do_crimson_file(file, param, logger=None, get_fcn=get_feedback_from_tweet, multi=True):
    ''' Get feedbacks from a twitter dump file
    :file: twitter file to process
    :param: dictionary of keyword parameters for get_opinion. Example get_opinion_parameters
    :get_fcn: function to retrieve individual feedback. Default to get_feedback_from_tweet
    :logger: logger to use. If None then print warnings, no lower level
    :multi boolean, whether to execute all file in parallel (processing pool). Default True
    '''
    raise NotImplementedError

def do_bazaar(file=None, dir=None, format=FORMAT_FILE, do_fcn=do_bazaar_file, list_file_fcn=None, thread=False, logger=None):
    ''' Create feedback outputs from bazaarvoice file or from directory
    :file: bazaarvoice file to process. If None, all bazaarvoice files in dir
    :dir: string, location to read bazaarvoice files from if file is None
    :format: suffix or iterable of suffix format to add to existing file name
    :do_fcn: function processing files. Default to do_bazaar_file
    :list_file_fcn: function to list bazaarvoice file. Default to bazaarapi.list_file
    :thread: boolean, whether to execute all file in parallel (threading). Default False
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if list_file_fcn is None:
        list_file_fcn = bazaarapi.list_file
    if dir is None:
        dir = bazaarapi.where()
    file_with_path = os.path.join(dir, file) if file and not os.path.isfile(file) else file
    files_todo = list_file_fcn(dir=dir, logger=logger) if not file else [file_with_path]
    files_todo = [f for f in files_todo if f+format not in list_file(dir=dir, logger=logger)]
    if not files_todo:
        msg = 'No file found to process. file=%s and dir=%s ' % (file, dir)
        if logger:
            logger.warning(msg)
        else:
            print(msg)
        return None
    param = get_opinion_parameters()
    if thread:
        pool = pathos.multiprocessing.ThreadingPool()
        results = pool.map(do_fcn, files_todo, [param]*len(files_todo), [logger]*len(files_todo))
        pool.close()
        pool.join()
        pool.clear()
    else:
        results = list(map(do_fcn, files_todo, [param]*len(files_todo), [logger]*len(files_todo)))
    files_done = [save_file(results[i], datestamp=files_todo[i], logger=logger, dir=dir) for i in range(len(results))]
    return files_done

def do_twitter(file=None, dir=None, format=FORMAT_FILE, do_fcn=do_twitter_file, list_file_fcn=None, thread=False, logger=None):
    ''' Create feedback outputs from twitter file or from directory
    :file: twitter file to process. If None, all twitter files in dir
    :dir: string, location to read twitter files from if file is None
    :format: suffix or iterable of suffix format to add to existing file name
    :do_fcn: function processing file. Default to do_twitter_file
    :list_file_fcn: function to list twitter files. Default to twitterapi.list_file
    :thread: boolean, whether to execute all file in parallel (threading). Default False
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if list_file_fcn is None:
        list_file_fcn = twitterapi.list_file
    return do_bazaar(file=file, dir=dir, format=format, do_fcn=do_fcn, list_file_fcn=list_file_fcn, thread=thread, logger=logger)

def do_crimson(file=None, dir=None, format=FORMAT_FILE, do_fcn=do_twitter_file, list_file_fcn=None, thread=False, logger=None):
    ''' Create feedback outputs from twitter file or from directory
    :file: twitter file to process. If None, all twitter files in dir
    :dir: string, location to read twitter files from if file is None
    :format: suffix or iterable of suffix format to add to existing file name
    :do_fcn: function processing file. Default to do_twitter_file
    :list_file_fcn: function to list twitter files. Default to twitterapi.list_file
    :thread: boolean, whether to execute all file in parallel (threading). Default False
    :logger: logger to use. If None then print warnings, no lower level
    '''
    raise NotImplementedError

def get_extract_for_click_bazaar(list_var=None, dir=None, dir_bazaar=None, datestamp=None, sep='|', logger=None, format=FORMAT_FILE):
    ''' Create a review extract for Qlik/SAS
    :list_var: list of variable from original review to retrieve
    :dir: str, directory to save the extract to
    :dir_bazaar: str, directory containing bazaarvoice feedback dump files
    :datestamp: string, prefix for file naming format YYYYMMDD, default to yesterday
    :sep: string, field delimiter for the output file. Defaul comma
    :keeper: gate keeper to avoid infinite looping
    :logger: logger to use. If None then print warnings, no lower level
    :format: string, suffix for file naming
    '''
    if list_var is None:
        list_var = ['Id', 'ProductId', 'SubmissionTime', 'ReviewText', 'Title', 'Rating']
    if dir is None:
        dir = bazaarapi.where()
    if dir_bazaar is None:
        dir_bazaar = bazaarapi.where()
    if datestamp is None:
        datestamp = bazaarapi.get_datestamp(datetime.datetime.now()-datetime.timedelta(days=1))
    bazaar_reviews = bazaarapi.load_file(dir=dir_bazaar)
    bazaar_feedback = [(id, sentence, feedback) for id, sentence, feedback in load_file(dir=dir_bazaar) if feedback]
    bazaar_reviews_subset = [[review[var] for var in list_var] for review in bazaar_reviews]
    df_reviews = pandas.DataFrame(bazaar_reviews_subset, columns=list_var)
    df_feedback = pandas.DataFrame(bazaar_feedback, columns=['Id', 'Sentence', 'Feedback'])
    temp_feedback = df_feedback.apply(lambda x: pandas.Series(x['Feedback']), axis=1).stack().reset_index(level=1, drop=True)
    temp_feedback.name = 'Feedback'
    df_feedback = df_feedback.drop('Feedback', axis=1).join(temp_feedback)
    df_feedback[['Term', 'Opinion', 'Score']] = pandas.DataFrame(df_feedback.Feedback.values.tolist(), index=df_feedback.index)
    df_feedback = df_feedback.merge(df_reviews, how='left', on='Id')
    file = os.path.join(dir, datestamp+format)
    df_feedback.Sentence = df_feedback.Sentence.apply(lambda str: str.replace('\r', '').replace('\n', '').replace('|', ' '))
    df_feedback.ReviewText = df_feedback.ReviewText.apply(lambda str: str.replace('\r', '').replace('\n', '').replace('|', ' '))
    df_feedback.Score = df_feedback.Score.apply(lambda x: 1 if x>0 else -1)
    df_feedback.to_csv(file, sep=sep, header=True, index=False)
    if logger:
        logger.info('Extract saved in %s' % file)
    return file

if __name__ == '__main__':

    format = '%(processName)s - %(asctime)s - %(filename)s - %(funcName)s - %(lineno)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=format)

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    #logger.addHandler(logging.StreamHandler())
    logger.info('Execution starts %s' % os.path.realpath(__file__))

    path_bazaar = r'/home/administrator/03-COLLECTED_DATA/01-BAZAARVOICE'
    do_bazaar(dir=path_bazaar, logger=logger)

    path_twitter = r'/home/administrator/03-COLLECTED_DATA/02-TWITTER'
    do_twitter(dir=path_twitter, logger=logger)

    path_twitter_huawei = r'/home/administrator/03-COLLECTED_DATA/03-HUAWEI'
    do_twitter(dir=path_twitter_huawei, logger=logger)

    path_twitter_apple = r'/home/administrator/03-COLLECTED_DATA/04-APPLE'
    do_twitter(dir=path_twitter_apple, logger=logger)

    path_crimson = r'/home/administrator/03-COLLECTED_DATA/05-CRIMSON'
    # do_crimson(dir=path_crimson, logger=logger)

    path_facebook = r'/home/administrator/03-COLLECTED_DATA/06-FACEBOOK'
    do_twitter(dir=path_facebook, logger=logger)
    
    path_instagram = r'/home/administrator/03-COLLECTED_DATA/07-INSTAGRAM'
    do_twitter(dir=path_instagram, logger=logger)
    
    path_youtube = r'/home/administrator/03-COLLECTED_DATA/07-YOUTUBE'
    do_twitter(dir=path_youtube, logger=logger)
    
    logger.info('Execution ends %s' % os.path.realpath(__file__))
