# -*- coding: utf-8 -*-
'''
feedback_test.py
~~~~~~~~~~~~~~~~
This module implements unit testing of the feedback module as well as bazaarapi and twitterapi modules
'''

import unittest   # unit testing framework
import warnings   # warning control
import numpy      # multi-dimensional arrays and matrices
import os         # miscellaneous operating system interfaces
import sys        # system-specific parameters and functions
import datetime   # basic date and time types
import io         # core tools for working with streams
import functools  # higher-order functions and operations on callable objects

warnings.simplefilter('ignore')

import bazaarapi  # user-defined, reviews using bazaarvoice api
import twitterapi # user-defined, tweet data
import feedback   # user defined, feedback retrieval from text data

def ignore_warnings(fn):
    ''' Helper decorator to remove warnings in unit tests '''
    @functools.wraps(fn)
    def do_test(self, *args, **kwargs):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            fn(self, *args, **kwargs)
    return do_test

def silence_prints(fn):
    class NullIO(io.StringIO):
        ''' Helper class to silent function '''
        def write(self, txt):
            pass
    ''' Helper decorator to prevent functions to print in unit tests '''
    @functools.wraps(fn)
    def do_test(*args, **kwargs):
        saved_stdout = sys.stdout
        sys.stdout = NullIO()
        try:
            result = fn(*args, **kwargs)
        except Exception as e:
            raise e
        finally:
            sys.stdout = saved_stdout
        return result
    return do_test

def clean_unittest(fn):
    ''' Helper decorator to prevent functions to print and remove warnings '''
    @functools.wraps(fn)
    @ignore_warnings
    @silence_prints
    def do_test(*args, **kwargs):
        return fn(*args, **kwargs)
    return do_test

class TestFeedback(unittest.TestCase):

    @clean_unittest
    def test01_Opinion01(self):
        ''' Test base case 1, 'This is a great phone' '''
        text1 = 'This is a great phone'
        text2 = 'Great phone'
        lex_pos = ['great']
        res1 = feedback.get_opinion(text1, lexicon_pos=lex_pos, lexicon_neg=[])
        res2 = feedback.get_opinion(text2, lexicon_pos=lex_pos, lexicon_neg=[])
        self.assertTrue(len(res1)==1)
        self.assertTrue(len(res2)==1)
        self.assertTrue(res1[0][0].lower()=='phone')
        self.assertTrue(res1[0][0].lower()==res2[0][0].lower())
        self.assertTrue(res1[0][1].lower()==lex_pos[0])
        self.assertTrue(res1[0][1].lower()==res2[0][1].lower())
        self.assertTrue(res1[0][2]>0)
        self.assertTrue(res2[0][2]>0)

    @clean_unittest
    def test02_Opinion02(self):
        ''' Test base case 2, 'This is a bad phone' '''
        text1 = 'This is a bad phone'
        text2 = 'Bad phone'
        lex_neg = ['bad']
        res1 = feedback.get_opinion(text1, lexicon_neg=lex_neg, lexicon_pos=[])
        res2 = feedback.get_opinion(text2, lexicon_neg=lex_neg, lexicon_pos=[])
        self.assertTrue(len(res1)==1)
        self.assertTrue(len(res2)==1)
        self.assertTrue(res1[0][0].lower()=='phone')
        self.assertTrue(res1[0][0].lower()==res2[0][0].lower())
        self.assertTrue(res1[0][1].lower()==lex_neg[0])
        self.assertTrue(res1[0][1].lower()==res2[0][1].lower())
        self.assertTrue(res1[0][2]<0)
        self.assertTrue(res2[0][2]<0)

    @clean_unittest
    def test03_Opinion03(self):
        ''' Test base case 3, 'The phone is outdated' '''
        text1 = 'Phone is outdated'
        text2 = 'The phone is outdated'
        lex_neg = ['outdated']
        res1 = feedback.get_opinion(text1, lexicon_neg=lex_neg, lexicon_pos=[])
        res2 = feedback.get_opinion(text2, lexicon_neg=lex_neg, lexicon_pos=[])
        self.assertTrue(len(res1)==1)
        self.assertTrue(len(res2)==1)
        self.assertTrue(res1[0][0].lower()=='phone')
        self.assertTrue(res1[0][0].lower()==res2[0][0].lower())
        self.assertTrue(res1[0][1].lower()==lex_neg[0])
        self.assertTrue(res1[0][1].lower()==res2[0][1].lower())
        self.assertTrue(res1[0][2]<0)
        self.assertTrue(res2[0][2]<0)

    @clean_unittest
    def test04_Opinion04(self):
        ''' Test weight case 1, 'The phone is very good' '''
        text0 = 'The phone is good'
        text1 = 'The phone is very good'
        text2 = 'This is a very good phone'
        lex_pos = ['good', 'very']
        res0 = feedback.get_opinion(text0, lexicon_pos=lex_pos, lexicon_neg=[])
        res1 = feedback.get_opinion(text1, lexicon_pos=lex_pos, lexicon_neg=[])
        res2 = feedback.get_opinion(text2, lexicon_pos=lex_pos, lexicon_neg=[])
        self.assertTrue(len(res1)==1)
        self.assertTrue(len(res2)==1)
        self.assertTrue(res1[0][0].lower()=='phone')
        self.assertTrue(res1[0][0].lower()==res2[0][0].lower())
        self.assertTrue(res1[0][1].lower()==lex_pos[0])
        self.assertTrue(res1[0][1].lower()==res2[0][1].lower())
        self.assertTrue(res1[0][2]==res2[0][2])
        self.assertTrue(res0[0][2]<res1[0][2])

    @clean_unittest
    def test05_Opinion05(self):
        ''' Test advanced case 1, 'Phone is outdated and bland' '''
        text1 = 'Phone is outdated and bland'
        lex_neg = ['outdated', 'bland']
        res1 = feedback.get_opinion(text1, lexicon_neg=lex_neg, lexicon_pos=[])
        self.assertTrue(len(res1)==2)
        self.assertTrue(res1[0][0].lower()=='phone')
        self.assertTrue(res1[0][0].lower()==res1[1][0].lower())
        self.assertTrue(res1[0][1].lower() in lex_neg)
        self.assertTrue(res1[1][1].lower() in lex_neg)
        self.assertFalse(res1[0][1].lower() == res1[1][1].lower())

    @clean_unittest
    def test06_Opinion06(self):
        ''' Test advanced case 2, 'Phone is outdated, slow and bland' '''
        text1 = 'Phone is outdated, slow and bland'
        lex_neg = ['outdated', 'slow', 'bland']
        res1 = feedback.get_opinion(text1, lexicon_neg=lex_neg, lexicon_pos=[])
        self.assertTrue(len(res1)==3)
        self.assertTrue(res1[0][0].lower()=='phone')
        self.assertTrue(res1[0][0].lower()==res1[1][0].lower())
        self.assertTrue(res1[1][0].lower()==res1[2][0].lower())
        self.assertTrue(res1[0][1].lower() in lex_neg)
        self.assertTrue(res1[1][1].lower() in lex_neg)
        self.assertTrue(res1[2][1].lower() in lex_neg)
        self.assertFalse(res1[0][1].lower() == res1[1][1].lower())
        self.assertFalse(res1[0][1].lower() == res1[2][1].lower())
        self.assertFalse(res1[1][1].lower() == res1[2][1].lower())

    @clean_unittest
    def test07_Opinion07(self):
        ''' Test weight case 2, 'The phone is great and very good' '''
        text1 = 'The phone is great and very good'
        lex_pos = ['very', 'good', 'great']
        res1 = feedback.get_opinion(text1, lexicon_pos=lex_pos, lexicon_neg=[])
        self.assertTrue(len(res1)==2)
        self.assertTrue(res1[0][0].lower()=='phone')
        self.assertTrue(res1[0][0].lower()==res1[1][0].lower())
        self.assertTrue(res1[0][1].lower() in lex_pos)
        self.assertTrue(res1[1][1].lower() in lex_pos)
        self.assertFalse(res1[0][1].lower() == res1[1][1].lower())
        _index = [each[1].lower() for each in res1].index('good')
        _index_no = (_index+1) % 2
        self.assertTrue(res1[_index][2]>res1[_index_no][2])

    @clean_unittest
    def test08_Opinion08(self):
        ''' Test combo case 2, 'This phone is great but battery is awful' '''
        text1 = 'This phone is great but battery is awful'
        lex_pos = ['great']
        lex_neg = ['awful']
        res1 = feedback.get_opinion(text1, lexicon_pos=lex_pos, lexicon_neg=lex_neg)
        self.assertTrue(len(res1)==2)
        _pos = [each[2]>0 for each in res1].index(True)
        _neg = [each[2]<0 for each in res1].index(True)
        self.assertTrue(res1[_pos][0].lower()=='phone')
        self.assertTrue(res1[_pos][1].lower()=='great')
        self.assertTrue(res1[_pos][2]>0)
        self.assertTrue(res1[_neg][0].lower()=='battery')
        self.assertTrue(res1[_neg][1].lower()=='awful')
        self.assertTrue(res1[_neg][2]<0)

    @clean_unittest
    def test09_Opinion09(self):
        ''' Test punctuation check, 'Battery is bad.' '''
        text1 = 'Battery is bad.'
        lex_pos = []
        lex_neg = ['bad']
        res1 = feedback.get_opinion(text1, lexicon_pos=lex_pos, lexicon_neg=lex_neg)
        self.assertTrue(len(res1)==1)

    @clean_unittest
    def test10_File01(self):
        ''' Test file WORD_POSITIVE can be imported '''
        test = feedback.load_lexicon(feedback.WORD_POSITIVE)
        self.assertTrue(len(test)>1)

    @clean_unittest
    def test11_File02(self):
        ''' Test file WORD_NEGATIVE can be imported '''
        test = feedback.load_lexicon(feedback.WORD_NEGATIVE)
        self.assertTrue(len(test)>1)

    @clean_unittest
    def test12_File03(self):
        ''' Test file WORD_STOPWORD can be imported '''
        test = feedback.load_lexicon(feedback.WORD_STOPWORD)
        self.assertTrue(len(test)>1)

    @clean_unittest
    def test13_File04(self):
        ''' Test file PROD_HIERARCHY can be imported '''
        test = feedback.load_product_hierarchy(feedback.PROD_HIERARCHY)
        self.assertTrue(len(test)>1)

    @clean_unittest
    def test14_Similarity01(self):
        ''' Test get_similarity_dic Levenshtein base case scenario , [features, features, feature] '''
        list1 = ['features', 'features', 'feature']
        param = dict(similarity=None, lowercase=True, lemmatizer=None)
        threshold1 = 0.5
        threshold2 = 1
        bag1, bag2, test1 = feedback.get_similarity_dic(list1, threshold=threshold1, return_bags=True, **param)
        test2 = feedback.get_similarity_dic(list1, threshold=threshold1, **param)
        test3 = feedback.get_similarity_dic(list1, threshold=threshold2, **param)
        self.assertTrue('features' in bag1)
        self.assertTrue('features' in bag2)
        self.assertTrue(test1=={'feature': 'features'})
        self.assertTrue(test1==test2)
        self.assertFalse(test1=={})

    @clean_unittest
    def test15_Similarity02(self):
        ''' Test get_similarity_dic Levenshtein conflict scenario, [recommend, recomand, command, recommend, command] '''
        list1 = ['recommend']*3 + ['recomand'] + ['command']*2
        param = dict(threshold=0.8, similarity=None, lowercase=True, lemmatizer=None)
        bag1, bag2, test1 = feedback.get_similarity_dic(list1, return_bags=True, **param)
        test2 = feedback.get_similarity_dic(list1, **param)
        self.assertTrue('recommend' in bag1)
        self.assertTrue('command' in bag1)
        self.assertTrue('recommend' in bag2)
        self.assertTrue('command' in bag2)
        self.assertTrue(len(test1)==1)
        self.assertTrue(test1=={'recomand': 'recommend'})

    @clean_unittest
    def test16_Similarity03(self):
        ''' Test get_similarity_dic Levenshtein transition scenario, [simple, simple, simpl, simplr] '''
        list1 = ['simple']*4 + ['simpler']*1 + ['simpl']*3 + ['simplr']*2
        param = dict(threshold=0.9, similarity=None, lowercase=False, lemmatizer=None)
        bag1, bag2, test1 = feedback.get_similarity_dic(list1, return_bags=True, **param)
        test2 = feedback.get_similarity_dic(list1, **param)
        self.assertTrue(set([test1[key] for key in test1])=={'simple'})
        self.assertTrue(test1==test2)
        self.assertTrue(len(bag1['simple'])==2)
        self.assertTrue('simpl' in [str for str, score in bag1['simple']])
        self.assertTrue('simpler' in [str for str, score in bag1['simple']])
        self.assertTrue(len(bag1['simpl'])==1)
        self.assertTrue('simplr' in [str for str, score in bag1['simpl']])
        self.assertTrue(len(bag1['simplr'])==1)
        self.assertTrue('simpler' in [str for str, score in bag1['simplr']])
        self.assertTrue(len(bag2['simplr'])==0)
        self.assertTrue(len(bag2['simpl'])==0)
        self.assertTrue(len(bag2['simple'])==4)

    @clean_unittest
    def test17_Similarity04(self):
        ''' Test get_similarity_dic Levenshtein lemma scenario, [batteries, battery, battery] '''
        list1 = ['batteries', 'battery', 'battery']
        param = dict(threshold=0.9, similarity=None, lowercase=False)
        bag1, bag2, test1 = feedback.get_similarity_dic(list1, lemmatizer=None, return_bags=True, **param)
        test2 = feedback.get_similarity_dic(list1, lemmatizer=None, return_bags=False, **param)
        bag3, bag4, test3 = feedback.get_similarity_dic(list1, lemmatizer=True, return_bags=True, **param)
        test4 = feedback.get_similarity_dic(list1, lemmatizer=True, return_bags=False, **param)
        self.assertTrue(test1=={})
        self.assertTrue(test1==test2)
        self.assertTrue(test3=={'batteries': 'battery'})
        self.assertTrue(test3==test4)

    @clean_unittest
    def test18_Similarity05(self):
        ''' Test get_similarity_dic with chunksize option '''
        list1 = ['batteries']*10 + ['batery'] *5 + ['battery']*2
        list1 += ['trash']*8 + ['rubbish']*8
        list1 += ['random']*4 + ['example']*4
        param = dict(threshold=0.5, similarity=None, lowercase=False, return_bags=False, lemmatizer=None)
        test1 = feedback.get_similarity_dic(list1, chunksize=3, **param)
        test2 = feedback.get_similarity_dic(list1, chunksize=2, **param)
        self.assertTrue('batery' in test1)
        self.assertTrue('battery' in test1)
        self.assertFalse('batery' in test2)
        self.assertFalse('battery' in test2)

    @clean_unittest
    def test19_Word2Vec01(self):
        ''' Test word2vec similarity function based on word2vec '''
        model = feedback.load_word2vec()
        string = 'example'
        string1 = 'battery'
        string2 = 'battery life'
        test1 = feedback.word2vec_similarity(string, string, model=model)
        self.assertTrue(test1==1)
        test2 = feedback.word2vec_similarity(string1, string2, model=model)
        self.assertTrue(test2>0)
        string3 = 'exist'
        string4 = 'definitly_doesnt_exist'
        test3 = feedback.word2vec_similarity(string3, string4, model=model)
        self.assertTrue(test3==0)
        list1 = ['polite']*4 + ['courteous']*1
        similarity_wrapper = feedback.word2vec_similarity_wrapper(model=model)
        param1 = dict(threshold=0.5, similarity=None, lowercase=False, lemmatizer=None)
        param2 = dict(threshold=0.5, similarity=similarity_wrapper, lowercase=False, lemmatizer=None)
        test4 = feedback.get_similarity_dic(list1, return_bags=False, **param1)
        test5 = feedback.get_similarity_dic(list1, return_bags=False, **param2)
        self.assertTrue(test4=={})
        self.assertTrue(test5=={'courteous': 'polite'})

    @clean_unittest
    def test20_SplitSentence01(self):
        ''' Test split_sentence base scenario '''
        text1 = 'I bought a phone last week. I am happy with it.'    # 2 sentences
        text2 = 'I bought a phone last week and I am happy with it.' # 1 sentence
        text3 = 'I bought a phone last week I am happy with it.'     # 2 sentences but typo
        res1 = feedback.split_sentence(text1)
        res2 = feedback.split_sentence(text2)
        res3 = feedback.split_sentence(text3)
        self.assertTrue(len(res1)==2)
        self.assertTrue(res1==['I bought a phone last week.', 'I am happy with it.'])
        self.assertTrue(len(res2)==1)
        self.assertTrue(res2==[text2])
        self.assertTrue(len(res3)==2)
        self.assertTrue(res3==['I bought a phone last week', 'I am happy with it.'])

    @clean_unittest
    def test21_GetFeedback01(self):
        ''' Test get_feedback and get_feedback_from_review on a simple document '''
        text = r'The screen is great but battery is awful. Overall I like the phone'
        param = feedback.get_opinion_parameters()
        review = dict(ReviewText=text, Id=None)
        res1 = feedback.get_feedback(text, None, param)
        res2 = feedback.get_feedback_from_review(review, param)
        self.assertTrue(len(res1)==2)
        self.assertTrue(len(res1[0])==3)
        self.assertTrue(len(res1[1])==3)
        self.assertTrue(len(res1[0][2])==2)
        self.assertTrue(len(res1[1][2])==1)
        self.assertTrue(res1==res2)

    @clean_unittest
    def test22_GetFeedback02(self):
        ''' Test do_bazaar_file on a made up review file '''
        text1 = r'The screen is great but battery is awful. Overall I like the phone'
        text2 = r'The phone is crap. Apple is better and faster. I regret my purchase'
        text3 = r'This document has no opinion'
        review1 = dict(ReviewText=text1, Id=1)
        review2 = dict(ReviewText=text2, Id=2)
        review3 = dict(ReviewText=text3, Id=3)
        list_of_dic = [review1, review2, review3]
        path = bazaarapi.where()
        datestamp = sys._getframe().f_code.co_name
        param = feedback.get_opinion_parameters()
        file = bazaarapi.save_file(list_of_dic, datestamp=datestamp, format='.todelete', dir=path)
        res = feedback.do_bazaar_file(file, param)
        os.remove(file)
        self.assertFalse(os.path.isfile(file))
        self.assertTrue(set([len(each) for each in res])==set([3]))

    @clean_unittest
    def test23_GetFeedback03(self):
        ''' Test do_bazaar on made up review files '''
        text1 = r'The screen is great but battery is awful. Overall I like the phone'
        text2 = r'The phone is crap. Apple is better and faster. I regret my purchase'
        text3 = r'This document has no opinion'
        text4 = r'Bixby is cool but old. This is a common sentence about the weather. Siri is working better'
        text5 = r'The frigde is very noisy'
        review1 = dict(ReviewText=text1, Id=1)
        review2 = dict(ReviewText=text2, Id=2)
        review3 = dict(ReviewText=text3, Id=3)
        review4 = dict(ReviewText=text4, Id=4)
        review5 = dict(ReviewText=text5, Id=5)
        list_of_dic1 = [review1, review2, review3]
        list_of_dic2 = [review4, review5]
        path = bazaarapi.where()
        datestamp = sys._getframe().f_code.co_name
        format1 = '.todelete_1'
        format2 = '.todelete_2'
        file1 = bazaarapi.save_file(list_of_dic1, datestamp=datestamp, format=format1, dir=path)
        file2 = bazaarapi.save_file(list_of_dic2, datestamp=datestamp, format=format2, dir=path)
        param = feedback.get_opinion_parameters()
        test_list_file = lambda dir, logger: [os.path.join(dir, file) for file in os.listdir(dir) if os.path.join(dir, file) in [file1, file2]]
        test = feedback.do_bazaar(dir=path, list_file_fcn=test_list_file)
        test_file_format = [os.path.basename(file) for file in test]
        os.remove(file1)
        os.remove(file2)
        self.assertTrue(len(test)==2)
        res1 = feedback.load_file(file=test[0])
        res2 = feedback.load_file(file=test[1])
        res3 = feedback.load_file(dir=path, format=tuple(test_file_format))
        for file in test:
            os.remove(file)
        self.assertTrue(set([len(each) for each in res1])==set([3]))
        self.assertTrue(set([len(each) for each in res2])==set([3]))
        self.assertTrue(set([len(each) for each in res3])==set([3]))
        self.assertTrue(len(res1+res2)==len(res3))

    @clean_unittest
    def test24_GetReview01(self):
        ''' Test bazaarapi.get_reviews and bazaarapi.get_daily_reviews '''
        path = bazaarapi.where()
        proxies = {} if path.startswith('/') else bazaarapi.PROXIES
        test1 = bazaarapi.get_reviews(keeper=1, proxies=proxies)
        test2 = bazaarapi.get_reviews(keeper=2, proxies=proxies)
        self.assertTrue(len(test1)>0)
        self.assertTrue(len(test2)>=len(test1))
        test3 = bazaarapi.get_daily_reviews(keeper=1, proxies=proxies)
        test4 = bazaarapi.get_daily_reviews(keeper=2, proxies=proxies)
        self.assertTrue(len(test3)>0)
        self.assertTrue(len(test4)>=len(test3))

    @clean_unittest
    def test25_LoadApi01(self):
        ''' Test twitterapi.load_api (skiping get_tweets tests to avoid limit rate errors) '''
        api = twitterapi.load_api

    @clean_unittest
    def test26_SaveFile01(self):
        ''' Test bazaarapi.save_file, bazaarapi.list_file and bazaarapi.load_file '''
        path = bazaarapi.where()
        proxies = {} if path.startswith('/') else bazaarapi.PROXIES
        test1 = bazaarapi.get_reviews(keeper=1, proxies=proxies)
        test1 = bazaarapi.get_reviews(keeper=1, proxies=proxies)
        datestamp = sys._getframe().f_code.co_name
        format = '.todelete'
        list1 = bazaarapi.list_file(dir=path, format=format)
        file1 = bazaarapi.save_file(test1, datestamp=datestamp, format=format, dir=path)
        list2 = bazaarapi.list_file(dir=path, format=format)
        test2 = bazaarapi.load_file(file=file1, dir=path, format=format)
        self.assertTrue(os.path.isfile(file1))
        self.assertTrue(len(list2)==len(list1)+1)
        self.assertTrue(file1 in list2)
        self.assertTrue(len(test1)==len(test2))
        os.remove(file1)
        self.assertFalse(os.path.isfile(file1))

    @clean_unittest
    def test27_IncludeThreshold01(self):
        ''' Test include_threshold get_similarity_dic option '''
        list = ['phone']*2 + ['iphone']
        threshold = feedback.get_similarity_score('phone', 'iphone')
        test1 = feedback.get_similarity_dic(list, threshold=threshold, include_threshold=True)
        test2 = feedback.get_similarity_dic(list, threshold=threshold, include_threshold=False)
        self.assertTrue('iphone' in test1)
        self.assertFalse('iphone' in test2)

    @clean_unittest
    def test28_GetBagThreading01(self):
        ''' Test get_bag threading option '''
        list = ['phone']*2 + ['iphone']
        list += ['simple']*4 + ['simpler']*1 + ['simpl']*3 + ['simplr']*2
        list = ['batteries']*10 + ['batery'] *5 + ['battery']*2
        list += ['trash']*8 + ['rubbish']*8
        list += ['random']*4 + ['example']*4
        param = dict(similarity=None, lowercase=True, lemmatizer=None)
        threshold = 0.7
        test1 = feedback.get_bag(list, param, threshold, chunksize=None, include_threshold=True, thread=True)
        test2 = feedback.get_bag(list, param, threshold, chunksize=None, include_threshold=True, thread=False)
        self.assertTrue(test1==test2)

    @clean_unittest
    def test29_DoBazaarFilePooling01(self):
        ''' Test do_bazaar_file multi processing option '''
        text1 = r'The screen is great but battery is awful. Overall I like the phone'
        text2 = r'The phone is crap. Apple is better and faster. I regret my purchase'
        text3 = r'This document has no opinion'
        review1 = dict(ReviewText=text1, Id=1)
        review2 = dict(ReviewText=text2, Id=2)
        review3 = dict(ReviewText=text3, Id=3)
        list_of_dic = [review1, review2, review3]
        path = bazaarapi.where()
        datestamp = sys._getframe().f_code.co_name
        param = feedback.get_opinion_parameters()
        file = bazaarapi.save_file(list_of_dic, datestamp=datestamp, format='.todelete', dir=path)
        res1 = feedback.do_bazaar_file(file, param, multi=False)
        res2 = feedback.do_bazaar_file(file, param, multi=True)
        os.remove(file)
        self.assertFalse(os.path.isfile(file))
        self.assertTrue(res1==res2)

    @clean_unittest
    def test30_DoBazaarThreading01(self):
        ''' Test do_bazaar thread option '''
        text1 = r'The screen is great but battery is awful. Overall I like the phone'
        text2 = r'The phone is crap. Apple is better and faster. I regret my purchase'
        text3 = r'This document has no opinion'
        text4 = r'Bixby is cool but old. This is a common sentence about the weather. Siri is working better'
        text5 = r'The frigde is very noisy'
        review1 = dict(ReviewText=text1, Id=1)
        review2 = dict(ReviewText=text2, Id=2)
        review3 = dict(ReviewText=text3, Id=3)
        review4 = dict(ReviewText=text4, Id=4)
        review5 = dict(ReviewText=text5, Id=5)
        list_of_dic1 = [review1, review2, review3]
        list_of_dic2 = [review4, review5]
        path = bazaarapi.where()
        datestamp = sys._getframe().f_code.co_name
        format1 = '.todelete_1'
        format2 = '.todelete_2'
        file1 = bazaarapi.save_file(list_of_dic1, datestamp=datestamp, format=format1, dir=path)
        file2 = bazaarapi.save_file(list_of_dic2, datestamp=datestamp, format=format2, dir=path)
        param = feedback.get_opinion_parameters()
        test_list_file = lambda dir, logger: [os.path.join(dir, file) for file in os.listdir(dir) if os.path.join(dir, file) in [file1, file2]]
        test1 = feedback.do_bazaar(dir=path, list_file_fcn=test_list_file, thread=False)
        self.assertTrue(len(test1)==2)
        res1 = feedback.load_file(file=test1[0])
        res2 = feedback.load_file(file=test1[1])
        res3 = feedback.load_file(dir=path, format=tuple(test1))
        for file in test1:
            os.remove(file)
        test2 = feedback.do_bazaar(dir=path, list_file_fcn=test_list_file, thread=True)
        self.assertTrue(len(test2)==2)
        res4 = feedback.load_file(file=test2[0])
        res5 = feedback.load_file(file=test2[1])
        res6 = feedback.load_file(dir=path, format=tuple(test2))
        for file in test2:
            os.remove(file)
        os.remove(file1)
        os.remove(file2)
        self.assertFalse(os.path.isfile(file1))
        self.assertFalse(os.path.isfile(file2))
        for file in test1+test2:
            self.assertFalse(os.path.isfile(file))
        self.assertTrue(res1==res4)
        self.assertTrue(res2==res5)
        self.assertTrue(res3==res6)

    @clean_unittest
    def test31_GetMaxId01(self):
        ''' Test twitterapi.get_max_id '''
        api = twitterapi.load_api()
        query = 'example'
        test = twitterapi.get_max_id(api, query)
        self.assertTrue(len(test)>0)

    @clean_unittest
    def test32_GetTweet01(self):
        ''' Test twitterapi.get_tweets '''
        api = twitterapi.load_api()
        query = 'example'
        test = twitterapi.get_tweets(api, query, keeper=1)
        self.assertTrue(len(test)>0)

    @clean_unittest
    def test33_SaveFile02(self):
        ''' Test twitterapi.save_file, twitterapi.list_file and twitterapi.load_file '''
        path = bazaarapi.where()
        api = twitterapi.load_api()
        query = 'example'
        test1 = twitterapi.get_tweets(api, query, keeper=1)
        format = '.todelete'
        datestamp = sys._getframe().f_code.co_name
        list1 = twitterapi.list_file(dir=path, format=format)
        file1 = twitterapi.save_file(test1, datestamp=datestamp, format=format, dir=path)
        list2 = twitterapi.list_file(dir=path, format=format)
        test2 = twitterapi.load_file(file=file1, dir=path, format=format)
        self.assertTrue(os.path.isfile(file1))
        self.assertTrue(len(list2)==len(list1)+1)
        self.assertTrue(file1 in list2)
        self.assertTrue(len(test1)==len(test2))
        os.remove(file1)
        self.assertFalse(os.path.isfile(file1))

    @clean_unittest
    def test34_LoadFile01(self):
        ''' Test bazaarapi.load_file using a list of files '''
        # TODO: rewrite this one, using twitterapi instead and get_yesterday
        path = bazaarapi.where()
        fct_stamp = sys._getframe().f_code.co_name
        format = '_' + fct_stamp + '.todelete'
        proxies = {} if path.startswith('/') else bazaarapi.PROXIES
        file1 = twitterapi.get_yesterday(dir=path, lag_day=1, format=format, keeper=1)
        file2 = twitterapi.get_yesterday(dir=path, lag_day=2, format=format, keeper=1)
        api = twitterapi.load_api()
        query = '#example'
        example_data = twitterapi.get_tweets(api, query, keeper=1, logger=None)
        file3 = twitterapi.save_file(example_data, datestamp=query, format=format, dir=path)
        datestamp1 = bazaarapi.get_datestamp(datetime.datetime.now() - datetime.timedelta(days=1))
        datestamp2 = bazaarapi.get_datestamp(datetime.datetime.now() - datetime.timedelta(days=2))
        datestamp3 = bazaarapi.get_datestamp_n(2)+('#sexample',)
        list1 = [file1, file2]
        list2 = [file1, file2, file3]
        data1 = twitterapi.load_file(file=file1)
        data2 = twitterapi.load_file(file=file2)
        data3 = twitterapi.load_file(file=list1)
        data4 = twitterapi.load_file(dir=path, format=format)
        data5 = twitterapi.load_file(file=list2)
        list_file1 = twitterapi.list_file(dir=path, datestamp=datestamp1, format=format)
        list_file2 = twitterapi.list_file(dir=path, datestamp=datestamp2, format=format)
        list_file3 = twitterapi.list_file(dir=path, datestamp=(datestamp1, datestamp2), format=format)
        list_file4 = twitterapi.list_file(dir=path, datestamp=datestamp3, format=format)
        list_file5 = twitterapi.list_file(dir=path, format=format)
        self.assertTrue(len(data1)>0)
        self.assertTrue(len(data2)>0)
        self.assertTrue(len(data3)>0)
        self.assertTrue(len(data1)+len(data2)==len(data3))
        self.assertTrue(data1+data2==data3)
        self.assertTrue(len(data5)==len(data4))
        self.assertTrue(file1 in list_file1)
        self.assertTrue(file2 in list_file2)
        self.assertTrue(file1 in list_file3 and file2 in list_file3)
        self.assertTrue(list_file5==list_file4)
        self.assertFalse(file3 in list_file1 or file3 in list_file2)
        os.remove(file1)
        os.remove(file2)
        os.remove(file3)

    @clean_unittest
    def test35_MeanSimilarityCluster01(self):
        ''' Test twitterapi.get_mean_similarity_cluster '''
        test1 = 'phone'
        test2 = 'pgone'
        test3 = 'iphone'
        list1 = [test1, test2]
        list2 = [test1, test2, test3]
        param_similarity = dict(similarity=None, lowercase=False, lemmatizer=False)
        res1 = twitterapi.get_mean_similarity_cluster(list1, param_similarity=param_similarity)
        res2 = twitterapi.get_mean_similarity_cluster(list2, param_similarity=param_similarity)
        sim1 = feedback.get_similarity_score(test1, test2, **param_similarity)
        sim2 = feedback.get_similarity_score(test1, test3, **param_similarity)
        sim3 = feedback.get_similarity_score(test2, test3, **param_similarity)
        self.assertTrue(res1==sim1)
        self.assertTrue(res2==sum([sim1, sim2, sim3])/3)

    @clean_unittest
    def test36_MeanSimilarityCluster02(self):
        ''' Test twitterapi.get_mean_similarity '''
        item1 = 'phone'
        item2 = 'pgone'
        item3 = 'iphone'
        item4 = 'polite'
        item5 = 'courteous'
        item6 = 'trash'
        item7 = 'rubbish'
        item8 = 'bin'
        clus1 = [item1, item2, item3]
        clus2 = [item4, item5]
        clus3 = [item6, item7, item8]
        param_similarity = dict(similarity=None, lowercase=False, lemmatizer=False)
        list0 = clus1+clus2+clus3
        cluster0 = [1]*len(clus1)+[2]*len(clus2)+[3]*len(clus3)
        list1 = [item1, item4, item6, item2, item5, item7, item3, item8]
        cluster1 = [1, 2, 3, 1, 2, 3, 1, 3]
        res1 = twitterapi.get_mean_similarity_cluster(clus1, param_similarity=param_similarity)
        res2 = twitterapi.get_mean_similarity_cluster(clus2, param_similarity=param_similarity)
        res3 = twitterapi.get_mean_similarity_cluster(clus3, param_similarity=param_similarity)
        test0_score, test0_unique = numpy.array([res1, res2, res3]), numpy.unique(cluster0)
        test1_score, test1_unique = twitterapi.get_mean_similarity(list0, cluster0, sample=10, param_similarity=param_similarity, multi=True)
        test2_score, test2_unique = twitterapi.get_mean_similarity(list0, cluster0, sample=10, param_similarity=param_similarity, multi=False)
        test3_score, test3_unique = twitterapi.get_mean_similarity(list1, cluster1, sample=10, param_similarity=param_similarity, multi=False)
        test4_score, test4_unique = twitterapi.get_mean_similarity(list1, cluster1, sample=10, param_similarity=param_similarity, multi=True)
        sim11 = feedback.get_similarity_score(item1, item2, **param_similarity)
        sim12 = feedback.get_similarity_score(item1, item3, **param_similarity)
        sim13 = feedback.get_similarity_score(item2, item3, **param_similarity)
        sim21 = feedback.get_similarity_score(item4, item5, **param_similarity)
        sim31 = feedback.get_similarity_score(item6, item7, **param_similarity)
        sim32 = feedback.get_similarity_score(item6, item8, **param_similarity)
        sim33 = feedback.get_similarity_score(item7, item8, **param_similarity)
        test5_score, test5_unique = numpy.array([sum([sim11, sim12, sim13])/3, sim21, sum([sim31, sim32, sim33])/3]), numpy.unique(cluster0)
        self.assertTrue((test0_score==test1_score).all())
        self.assertTrue((test0_score==test2_score).all())
        self.assertTrue((test0_score==test3_score).all())
        self.assertTrue((test0_score==test4_score).all())
        self.assertTrue((test0_score==test5_score).all())
        self.assertTrue((test0_unique==test1_unique).all())
        self.assertTrue((test0_unique==test2_unique).all())
        self.assertTrue((test0_unique==test3_unique).all())
        self.assertTrue((test0_unique==test4_unique).all())
        self.assertTrue((test0_unique==test5_unique).all())

if __name__ == '__main__':

    unittest.main(exit=False)
