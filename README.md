# Reviews and aspect based opinion

This repository implements a methodology to analyse reviews beyond overall sentiment and polarity scores
and understand specifically which aspects of a particular products or services are rated highly or not

Main functions are implemented in *feedback.py*. An example of usage is available in the included notebook and its export in html format. The repository also contains some api related modules to access, automate data collection and processing

### Background

Feedback data is widely available online either in the form of direct feedback from the customers (example reviews) or indirect feedback (example tweets). Beyond overall sentiment scoring, it is important to understand what are the relevant drivers behind good, bad as well as mixed feedbacks

* if a movie is badly rated, is it because of its acting or its scenario?
* if a phone is highly rated, is it because of its screen or its battery life?
* if a taxi ride is badly rated, is it because of the driver converstation or dangerous driving?

The basic idea is simple and intuitive enough: identify opinion words within a sentence and the word the opinion refer to. 
A simple pipeline of modelling using tools like Spacy can be build in Python to help extrapolating information out of a sentence (example, which words are adjectives or how each word refer to the other in a sentence)

![visual](https://bitbucket.org/daiScience/text-reviews-and-aspect-based-opinion/downloads/visual.png)