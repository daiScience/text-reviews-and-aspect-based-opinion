# -*- coding: utf-8 -*-
'''
twitterapi.py
~~~~~~~~~~~~~
This module implements download and management of tweets using the twitter api in Python
This script will download previous day's tweets using twitter api
'''

import logging    # logging facility
logger_module = logging.getLogger(__name__)
logger_module.setLevel(logging.INFO)

import tweepy     # library for accessing the twitter api
# Note on python 3.7 https://github.com/tweepy/tweepy/issues/1017
# Note on tweepy: manual updates of code. Related to requests ssl certificate
#   1. (/home/administrator/anaconda3/lib/python3.7/site-packages/tweepy) binder.py
#   1. addition of "session.trust_env = False" after line "session = requests.Session()", line 40
#   2. (/home/administrator/anaconda3/lib/python3.7/site-packages/tweepy) auth.py
#   2. addition of "verify=False" in "resp = requests.post(" line 163 to 166
#   3. (/home/administrator/anaconda3/lib/python3.7/site-packages/tweepy)
#   3. addition of "session.trust_env = False" after line "self.session = requests.Session()", line 227
import datetime   # basic date and time types
import sys        # system-specific parameters and functions
import os         # miscellaneous operating system interfaces
import requests   # http requests for humans
import numpy      # multi-dimensional arrays and matrices
import matplotlib # base plotting
import seaborn    # statistical data visualization
import sklearn    # machine learning library
import hdbscan    # density based clustering
import pathos     # parallel graph management, execution in heterogeneous computing

import bazaarapi  # user-defined, reviews using bazaarvoice api
import feedback   # user defined, feedback retrieval from text data

# replace by empty dictionary if on server side, as address is whitelisted
PROXIES = bazaarapi.PROXIES

# twitter api keys and tokens
TWITTER_ACCESS = dict(
    TWITTER_API_KEY = ''
    , TWITTER_SECRET_KEY = ''
    , TWITTER_ACCESS_TOKEN = ''
    , TWITTER_SECRET_TOKEN = '')

# dump file naming convention
FORMAT_DAILY_FILE = '_TWITTER_DAY.json'
FORMAT_SPAM_FILE = '_TWITTER_SPAM.json'

def load_api(tokens=TWITTER_ACCESS, parser=None, wait=False, timeout=200, compression=True):
    ''' Load the twitter API
    :tokens: dictionary of twitter api keys and token
    :parser: parser to use instead of default tweepy.models.Status objects
    :wait: bool, whether to wait if limit is reach
    :timeout: delay before consider the request as timed out, seconds
    '''
    if parser is None:
        parser = tweepy.parsers.JSONParser()
    auth = tweepy.OAuthHandler(tokens.get('TWITTER_API_KEY'), tokens.get('TWITTER_SECRET_KEY'))
    auth.set_access_token(tokens.get('TWITTER_ACCESS_TOKEN'), tokens.get('TWITTER_SECRET_TOKEN'))
    return tweepy.API(auth, parser=parser, wait_on_rate_limit=wait, timeout=timeout, compression=compression)

@bazaarapi.retry(Exception, tries=20, logger=logger_module)
@bazaarapi.retry(tweepy.RateLimitError, delay=60*15+2, backoff=1, logger=logger_module)
def search_tweets(api, query, count=100, max_id=None, tweet_mode='extended', exclude_retweets=True):
    ''' Query the twitter api using the twitter wrapper
    :api: tweety api object
    :query: text to search on twitter
    :count: number of tweets to collect, api maximum is 100
    :max_id: only return statuses with an ID lower than (order than) the specified ID
    :tweet_mode: api option, 'extended' does not truncate tweet (retweets are still truncated)
    :exclude_retweets: api query filtering, whether to exclude retweets in results
    '''
    q = query+' -filter:retweets' if exclude_retweets else query
    if max_id is not None:
        search = api.search(q=q, count=count, tweet_mode=tweet_mode, max_id=max_id)
    else:
        search = api.search(q=q, count=count, tweet_mode=tweet_mode)
    return search

def to_datetime(date_text):
    ''' Helper function to convert date and time text from twitter api to python datetime '''
    return datetime.datetime.strptime(date_text, '%a %b %d %H:%M:%S %z %Y')

@bazaarapi.retry(Exception, tries=20, logger=logger_module)
@bazaarapi.retry(tweepy.RateLimitError, delay=60*15+2, backoff=1, logger=logger_module)
def get_max_id(api, query, date=None, count=50):
    ''' Get maximum id related to a query up until a given datetime date, default to yesterday's last id
    :api: tweety api object
    :query: text to search on twitter
    :date: data datetime format, default to today for yesterday last id
    :count: number of tweet to retrieve to get max_id. >1 is needed because of filter in api.search
    '''
    if date is None:
        date = datetime.datetime.now()
    date_str = '{0}-{1:0>2}-{2:0>2}'.format(date.year, date.month, date.day)
    tweets = api.search(q=query+' -filter:retweets', count=count, until=date_str)
    created = to_datetime(tweets.get('statuses')[0].get('created_at'))
    return (tweets.get('statuses')[0].get('id'), date, created)

def get_tweets(api, query, date=None, lang='en', keeper=100000, logger=None):
    ''' Get tweets related to a query on a given date, paginating backward from last tweet of day
    :api: tweety api object
    :query: text to search on twitter
    :date: data datetime format, None for yesterday's tweets
    :lang: str, only keep tweets language. None for no filtering, i.e. all tweets. Default 'en' for english
    :keeper: gate keeper to avoid infinite looping
    :logger: logger to use. If None then print warnings, no lower level
    '''
    max_id, _, tweet_date = get_max_id(api, query, date=date)
    tweets = []
    min_time = tweet_date
    for i in range(keeper):
        start = len(tweets)
        tweets += search_tweets(api, query, max_id=max_id).get('statuses')
        dic = {tweet.get('id'): tweet.get('created_at') for tweet in tweets}
        max_id = min(dic)
        min_time = to_datetime(dic.get(max_id))
        max_id -= 1
        if start==len(tweets) or not min_time.date()==tweet_date.date():
            break
    if i+1==keeper:
        msg = 'Exiting after %s page of api results' % (i+1)
        if logger:
            logger.warning(msg)
        else:
            print(msg)
    results = [t for t in tweets if to_datetime(t.get('created_at')).date()==tweet_date.date()]
    return results if lang is None else [tweet for tweet in results if tweet.get('lang')==lang]

def save_file(list_of_dic, datestamp=None, format=FORMAT_DAILY_FILE, dir=None, logger=None):
    ''' Save list of dictionaries into a dump json file
    :list_of_dic: dictionaries to save
    :datestamp: string, prefix for file naming format YYYYMMDD, default to yesterday
    :format: suffix format to save
    :dir: string, location to save file in
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if dir is None:
        dir = bazaarapi.where()
    return bazaarapi.save_file(list_of_dic, datestamp=datestamp, format=format, dir=dir, logger=logger)

def list_file(dir=None, datestamp=None, format=FORMAT_DAILY_FILE, logger=None):
    ''' List all twitter dump file in directory
    :dir: directory to list dump file from
    :datestamp: iterable of prefix of file to list
    :format: iterable of naming file format to list
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if dir is None:
        dir = bazaarapi.where()
    return bazaarapi.list_file(dir=dir, datestamp=datestamp, format=format, logger=logger)

def load_file(file=None, sample_size=None, dir=None, format=FORMAT_DAILY_FILE, logger=None):
    ''' Load tweets from file
    :file: file to load
    :sample_size: if file is None, maximum amount of reviews to load
    :dir: string, if file is None, location to read files from
    :format: if file is None, iterable of format to load
    :logger: logger to use. If None then print warnings, no lower level
    '''
    if dir is None:
        dir = bazaarapi.where()
    return bazaarapi.load_file(file=file, sample_size=sample_size, dir=dir, format=format, logger=logger)

def list_spam(dir=None, datestamp=None, format=FORMAT_SPAM_FILE, logger=None):
    ''' List all spam dump file in directory
    :dir: directory to list dump file from
    :datestamp: iterable of prefix of file to list
    :format: iterable of naming file format to list
    :logger: logger to use. If None then print warnings, no lower level
    '''
    return list_file(dir=dir, datestamp=datestamp, format=format, logger=logger)

def load_spam(file=None, sample_size=None, dir=None, format=FORMAT_SPAM_FILE, remove_duplicates=True, logger=None):
    ''' Load spams from file
    :file: file to load
    :sample_size: if file is None, maximum amount of reviews to load
    :dir: string, if file is None, location to read files from
    :format: if file is None, iterable of format to load
    :remove_duplicates: boolean, whether to remove duplicates in the list or not. Default True
    :logger: logger to use. If None then print warnings, no lower level
    '''
    spams = load_file(file=file, sample_size=sample_size, dir=dir, format=format, logger=logger)
    if not remove_duplicates:
        return spams
    return list({dic.get('id'): dic for dic in spams}.values())

def get_yesterday(dir=None, lag_day=1, query='example', lang='en', format=FORMAT_DAILY_FILE, keeper=100000, logger=None):
    ''' Download and save yesterday's tweets
    :dir: string, if file is None, directory to save file to
    :lag_day: Number of days ago to get tweets for (1 yesterday, 2 two days ago etc). Default to 1 for yesterday
    :query: text to search on twitter
    :lang: str, only keep tweets language. None for no filtering, i.e. all tweets. Default 'en' for english
    :format: get_daily_reviews parameter, string, suffix for file naming
    :keeper: gate keeper to avoid infinite looping
    :logger: logger to use. If None then print warnings, no lower level
    '''
    api = load_api()
    date = datetime.datetime.now() - datetime.timedelta(days=lag_day-1)
    dateforstamp = date - datetime.timedelta(days=1)
    datestamp = bazaarapi.get_datestamp(dateforstamp)
    data = get_tweets(api, query=query, date=date, lang=lang, keeper=keeper, logger=logger)
    file = save_file(data, datestamp=datestamp, format=format, dir=dir, logger=logger)
    if logger:
        logger.info('Tweets for %s day ago downloaded and saved in %s' % (lag_day, file))
    return file

def get_history(dir=None, end=8, query='example', lang='en', format=FORMAT_DAILY_FILE, keeper=100000, logger=None):
    ''' Download and save last days
    :dir: string, if file is None, directory to save file to
    :end: end of range to get twitter history for. 0 for yesterday, 1 for day before etc
    :query: text to search on twitter
    :lang: str, only keep tweets language. None for no filtering, i.e. all tweets. Default 'en' for english
    :format: get_daily_reviews parameter, string, suffix for file naming
    :keeper: gate keeper to avoid infinite looping
    :logger: logger to use. If None then print warnings, no lower level
    '''
    return [get_yesterday(dir=dir, lag_day=i, query=query, format=format, keeper=keeper, logger=logger) for i in range(0, end+1)]

def update_dic(dic, key, value):
    ''' Helper function to update and return dic with updated key of value '''
    dic.update({key: value})
    return dic

def get_mean_similarity_cluster(text, param_similarity=None):
    ''' Compute average similarity between text within cluster for a given cluster. Default Levenshtein similarity from feedback
    :text: list or iterable containing strings
    :cluster: list or iterable representing cluster of each text
    :cluster_number: int, cluster number to get mean similarity for
    :sample: int, sample size to compute similarity on within each cluster> none for all text
    :param_similarity: dictionary, parameter for feedback.get_similarity_score
    '''
    if param_similarity is None:
        param_similarity = dict(similarity=None, lowercase=False, lemmatizer=False)
    return numpy.mean([feedback.get_similarity_score(text[i], text[j], **param_similarity)
                       for i in range(len(text)) for j in range(i+1, len(text))])

def get_mean_similarity(text, cluster, sample=100, param_similarity=None, multi=True):
    ''' Compute average Levenshtein similarity between text within cluster for all clusters
    :text: list or iterable containing strings
    :cluster: list or iterable representing cluster of each text
    :sample: int, sample size to compute similarity on within each cluster. none for all text
    :param_similarity: dictionary, parameter for feedback.get_similarity_score (similarity, lowercase, lemmatizer)
    :multi: boolean, whether to execute all file in parallel (processing pool). Default True
    '''
    if param_similarity is None:
        param_similarity = dict(similarity=None, lowercase=False, lemmatizer=False)
    cluster_unique = numpy.unique(cluster)
    text_array = numpy.array(text)
    text_cluster = [numpy.random.choice(text_array[cluster==i], min(sample, len(text_array[cluster==i])), replace=False)
                    for i in cluster_unique]
    if multi:
        pool = pathos.multiprocessing.ProcessingPool()
        score = pool.map(get_mean_similarity_cluster, text_cluster, [param_similarity]*len(cluster_unique))
        pool.close()
        pool.join()
        pool.clear()
    else:
        score = [get_mean_similarity_cluster(text_cluster[i], param_similarity=param_similarity) for i in range(len(cluster_unique))]
    return score, cluster_unique

def get_spams_from_dic(list_of_dic, param_vect=None, param_svd=None, param_clust=None, param_mean_similarity=None, threshold=0.75, logger=None):
    ''' Retrieve twitter spams from a list of tweets
    :list_of_dic: list of dictionary, representation of tweets
    :param_vect: dictionary of parameter for bag of word vectorization part, CountVectorizer
    :param_svd: dictionary of parameter for dimension reduction part, TruncatedSVD
    :param_clust: dictionary of parameter for clustering part, HDBSCAN
    :param_mean_similarity: dictionary of parameter for Levenshtein similarity, get_mean_similarity
    :threshold: float, value to filter spams within each cluster, notion of incluster distance
    :logger: logger to use. If None then print warnings, no lower level
    '''
    text = [tweet.get('full_text') for tweet in list_of_dic]
    if logger:
        logger.debug('Calculating bag of words from text data')
    if param_vect is None:
        param_vect = dict(stop_words=None, tokenizer=None, lowercase=False, ngram_range=(1, 4), max_features=None)
    vect = sklearn.feature_extraction.text.CountVectorizer(**param_vect)
    vect_out = vect.fit_transform(text)
    if logger:
        logger.debug('Reducing dimensions of the bag of words')
    if param_svd is None:
        param_svd = dict(n_components=50, random_state=1)
    svd = sklearn.decomposition.TruncatedSVD(**param_svd)
    svd_out = svd.fit_transform(vect_out)
    if logger:
        logger.debug('Clustering the reduced vectors')
    if param_clust is None:
        param_clust = dict(min_cluster_size=5, metric='euclidean', algorithm='best', core_dist_n_jobs=-1, allow_single_cluster=True, prediction_data=False)
    clust = hdbscan.HDBSCAN(**param_clust)
    clust_out = clust.fit(svd_out)
    if logger:
        logger.debug('Calculating similarity within cluster')
    if param_mean_similarity is None:
        param_similarity = dict(similarity=None, lowercase=False, lemmatizer=False)
        param_mean_similarity = dict(sample=100, param_similarity=param_similarity, multi=True)
    cluster_score, cluster_unique = get_mean_similarity(text, clust.labels_, **param_mean_similarity)
    if logger:
        logger.debug('Creating output')
    score_dic = {cluster_unique[i]: cluster_score[i] for i in range(len(cluster_score))}
    idx_cluster = numpy.array(cluster_unique)[numpy.array(cluster_score)>=threshold]
    idx_list_of_dic = (numpy.array(clust.labels_)[:, None] == idx_cluster).any(axis=-1)
    return [update_dic(update_dic(list_of_dic[i], 'spam_cluster', int(clust.labels_[i]))
                       , 'spam_cluster_score', float(score_dic.get(clust.labels_[i])))
            for i in range(len(list_of_dic)) if idx_list_of_dic[i]]

def set_plot_param(plot_figsize=(18.0, 10.0)):
    ''' Setting some default plot parameters around resizing figure and figure elements '''
    seaborn.set(context='notebook', style='white', palette='colorblind', rc={'figure.figsize':plot_figsize})
    params = {
        'figure.figsize': plot_figsize
        , 'figure.titlesize': 20
        , 'axes.labelsize': 16
        , 'axes.facecolor': 'white'
        , 'axes.titlepad': 15.0
        , 'axes.titlesize': 20
        , 'axes.labelpad': 8
        , 'axes.edgecolor': 'black'
        , 'axes.linewidth': 0.5
        , 'xtick.labelsize': 14
        , 'ytick.labelsize': 14
        , 'legend.fontsize': 14
        }
    matplotlib.rcParams.update(params)
    return None

def get_elbow_curve(x, y, first=None, last=None, plot=True):
    ''' Return elbow value of a curve, maximum distance between y and line from first and last point https://stackoverflow.com/questions/2018178/
    :x: x values of curve coordinates
    :y: y values of curve coordinates
    :first: first point of the curve to consider. Default None for first in serie
    :last: last point of the curve to consider. Default None for last in serie
    :plot: boolean, whether to draw the graph and visualise results. Default True
    '''
    if first is None:
        first = (x[0], y[0])
    if last is None:
        last = (x[-1], y[-1])
    index_first = list(x).index(first[0])
    index_last = list(x).index(last[0])
    line_coefficient = numpy.polyfit(*zip(first, last), 1)
    line_polynomial = numpy.poly1d(line_coefficient)
    line_value = line_polynomial(x)
    elbow = index_first+numpy.argmax(numpy.absolute(y-line_value)[index_first:index_last])
    if plot:
        set_plot_param()
        matplotlib.pyplot.plot(x, y, marker='x', color='lightblue')
        matplotlib.pyplot.plot(x, y, linestyle='-', color='lightblue')
        matplotlib.pyplot.plot(x, line_value, linestyle='-', color='lightgrey')
        matplotlib.pyplot.plot(x[elbow], y[elbow], marker='o', color='r')
    return x[elbow], y[elbow]

def get_spams(dir=None, n=14, start=1, format_in=FORMAT_DAILY_FILE, format_out=FORMAT_SPAM_FILE, logger=None
             , param_vect=None, param_svd=None, param_clust=None, param_mean_similarity=None, threshold=0.75):
    ''' Create a file containing spams from a list of twitter data
    :dir: string, if file is None, directory to load twitter files from and to save spam file to
    :n: bazaarapi.get_datestamp_n parameter, number of days to retrieve datestamp for
    :start: bazaarapi.get_datestamp_n parameter, number of days away to look at. Default 1 for starting yesterday
    :format_in: suffix format for file to load to save
    :format_out: suffix format to save spam file with
    :logger: logger to use. If None then print warnings, no lower level
    :param_vect: get_spams_from_dic parameter, dictionary of parameter for bag of word vectorization part, CountVectorizer
    :param_svd: get_spams_from_dic parameter, dictionary of parameter for dimension reduction part, TruncatedSVD
    :param_clust: get_spams_from_dic parameter, dictionary of parameter for clustering part, HDBSCAN
    :param_mean_similarity: get_spams_from_dic parameter, dictionary of parameter for Levenshtein similarity, get_mean_similarity
    :threshold: get_spams_from_dic parameter, value to filter spams within each cluster, notion of incluster distance
    '''
    if logger:
        logger.info('Retrieving spams from tweets')
    if dir is None:
        dir = bazaarapi.where()
    datestamp_tuple = bazaarapi.get_datestamp_n(n, start=start)
    datestamp = datestamp_tuple[-1]
    files = list_file(dir=dir, datestamp=datestamp_tuple, format=format_in, logger=logger)
    if not len(files)==len(datestamp_tuple):
        msg = 'Loading %s files instead of expected %s. datestamp_tuple=%s and files=%s' % (len(files), len(datestamp_tuple), datestamp_tuple, files)
        if logger:
            logger.warning(msg)
        else:
            print(msg)
    data = load_file(file=files, dir=dir, format=format_in, logger=logger)
    param_spam = dict(param_vect=param_vect, param_svd=param_svd, param_clust=param_clust, param_mean_similarity=param_mean_similarity)
    spams = get_spams_from_dic(data, threshold=threshold, **param_spam)
    filename = save_file(spams, datestamp=datestamp, format=format_out, dir=dir, logger=logger)
    return filename

if __name__ == '__main__':

    format = '%(processName)s - %(asctime)s - %(filename)s - %(funcName)s - %(lineno)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=format)

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    #logger.addHandler(logging.StreamHandler())
    logger.info('Execution starts %s' % os.path.realpath(__file__))

    server_path = r'/home/administrator/03-COLLECTED_DATA/02-TWITTER'
    dir = server_path if os.path.isdir(server_path) else bazaarapi.where()
    file = get_yesterday(dir=dir, logger=logger)
    spam = get_spams(dir=dir, n=14, start=1, threshold=0.75, logger=logger)

    logger.info('Execution ends %s' % os.path.realpath(__file__))
